package it.unipd.dei.ims.blanco.algorithm.main;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.jena.ext.com.google.common.base.Stopwatch;

import it.unipd.dei.ims.blanco.algorithm.offline.RDFConvertionAlgorithmsToolkit;
import it.unipd.dei.ims.blanco.algorithm.offline.RDFDatabaseToTriplesTRECDocumentsConvertionPhase;
import it.unipd.dei.ims.blanco.algorithm.offline.TrecIndexPhase;
import it.unipd.dei.ims.blanco.algorithm.offline.UriDatabasePurifier;
import it.unipd.dei.ims.blanco.algorithm.online.AnswerSubgraphGenerationPhase;
import it.unipd.dei.ims.blanco.algorithm.online.AnswerSubgraphsRankingPhase;
import it.unipd.dei.ims.blanco.algorithm.online.FromAnswerSubgraphToTrecDocumentsPhase;
import it.unipd.dei.ims.blanco.algorithm.online.IndexerDirectoryOfTRECFiles;
import it.unipd.dei.ims.blanco.algorithm.online.MatchingTripleRetrievalPhase;
import it.unipd.dei.ims.blanco.algorithm.online.QueryGraphGenerationPhase;
import it.unipd.dei.ims.blanco.algorithm.online.RjDocumentCreationPhase;
import it.unipd.dei.ims.datastructure.ConnectionHandler;
import it.unipd.dei.ims.datastructure.DatabaseState;
import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;

/** This is the main control class of the Blanco algorithm. 
 * It recreates the Blanco ALgorithm procedure, so you can execute everything 
 * altogether.
 * 
 * */
public class BlancoMainClass {

	/** Timer to control the execution of the single algorithms.
	 * */
	protected static Stopwatch singleMethodTimer;

	/** Timer to control the all around execution.
	 * */
	protected static Stopwatch allAroundTimer;

	/** The query to execute */
	protected String query;

	/** Id of the query the class is executing. 
	 * */
	protected String queryId;

	/** Path of the main query directory, where all the results and
	 * data structures are stored.
	 * */
	protected String mainQueryDirectory;

	protected String outputTimeFilePath;

	boolean uriDatabasePurifierFlag, 
	rdfConvertionAlgorithmFlag, 
	rdfDatabaseToTriplesTRECDocumentConvertionFlag, 
	trecIndexFlag,
	matchingTripleRetrievalFlag, 
	queryGraphGenerationFlag, 
	answerSubgraphGenerationFlag, 
	fromAnswerSubgraphToTRECDocumentsFlag, 
	indexerDirectoryOfTRECFilesFlag, 
	rjDocumentCreationFlag, 
	answerSubgraphRankingFlag,
	originalMatchingTripleAlgorithmFlag,
	multipleTimesFlag;

	protected FileWriter fWriter;
	protected BufferedWriter timeWriter;
	
	protected String jdbcConnectionString;
	protected Connection connection;
	
	protected Map<String, String> map;
	
	/** Direcotry of the algorithm SLM.
	 * */
	protected String mainAlgorithmDirectory;
	
	/**Schema of the database we are working on.
	 * */
	protected String schema;

	public BlancoMainClass() {
		this.setup();
	}

	/** Deal with the properties and the fields of the object at 
	 * its creation.
	 * */
	protected void setup() {
		try {

			map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/main.properties");
			//configure all the labels that we are going to need
			this.configureLabels(map);
			
			this.mainQueryDirectory = map.get("main.query.directory");
			
			File f = new File(this.mainQueryDirectory);
			if(!f.exists()) {
				f.mkdirs();
			}
			//get the main algorithm directory
			this.mainAlgorithmDirectory = f.getParent();
			
			/*Set the schema of the database we are using. This
			 * will be useful in other classes during the process to address the
			 * correct SQL tables.
			 * */
			this.schema = map.get("schema");
			DatabaseState.setSchema(this.schema);
			
			
			//we create the file where we will write all the information about this execution
			String outputTimeFilePath = (new File(this.mainQueryDirectory)).getParent() +
					"/log/";
			File g = new File(outputTimeFilePath);
			g.mkdirs();
			outputTimeFilePath += this.schema + "_times.txt";
			this.fWriter = new FileWriter(outputTimeFilePath, true);
			this.timeWriter = new BufferedWriter(fWriter);			
			singleMethodTimer = Stopwatch.createUnstarted();
			allAroundTimer = Stopwatch.createUnstarted();
			
			this.jdbcConnectionString = map.get("jdbc.connection.string");
			this.multipleTimesFlag = Boolean.parseBoolean( map.get("multiple.queries") );
			
			//set the size of the database (useful in some cases)
			//where the algorithm changes based on the size
			String size_ = map.get("database.size");
			size_ = (size_!= null) ? size_ : "big";
			DatabaseState.setSize(size_);
		
			//set the kind of database we are using.
			//this is necessary to algorithms that need to translate 
			//graphs to documents
			String dbName = map.get("rdf.database.type");
			dbName = (dbName!= null) ? dbName : "default";
			DatabaseState.setType(dbName);
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public void close() {
		try {
			this.connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/** executes the Blanco algorithm once
	 * */
	public void executeBlancoAlgorithm() throws IOException {
		this.executeBlancoAlgorithm(false);
	}

	
	public void executeOfflinePhasesBlancoAlgorithm() throws IOException {
		
		allAroundTimer.start();

		// ***** OFFLINE PHASE ***** // (only once)
		//purification of database
		this.phase1();
		this.phase2();
		this.phase3();
		this.phase4();
		timeWriter.flush();
		
	}
	
	
	/** Method to execute the phases of the algorithm.
	 * */
	public void executeBlancoAlgorithm(boolean multipleTimes) throws IOException {

		System.out.println("Now beginning the Blanco procedure to answer to a query");

		this.configureLabels(map);
		
		allAroundTimer.start();

		// ***** OFFLINE PHASE ***** // (only once)
		//purification of database
		this.phase1();
		this.phase2();
		this.phase3();
		this.phase4();
		timeWriter.flush();

		// ***** end of the offline phase *****

		// ***** ON-LINE PHASE ***** //
		timeWriter.write("\n***********\n\nExecuting now the on-line part of query " + this.queryId + "\n"
				+ "keyword query: \n" + query + "\n\n");
		System.out.println("\n***********\n\nExecuting now the on-line part of query " + this.queryId + "\n"
				+ "keyword query: \n" + query + "\n\n");
		this.phase5(multipleTimes);
		this.phase6(multipleTimes);
		this.phase7(multipleTimes);
		this.phase8(multipleTimes);
		this.phase9(multipleTimes);
		this.phase10(multipleTimes);
		this.phase11(multipleTimes);

		System.out.println("the total time required to compute this query was: " + allAroundTimer.stop());
		timeWriter.write("the total time required to compute the query " + queryId 
				+ " was: " + allAroundTimer);
		allAroundTimer.reset();
		timeWriter.newLine();
//		timeWriter.close();
	}


	/** Executes the algorithm multiple times. Once for
	 * every query provided in the query_list.properties file.
	 * The key is used as query id to build up the directories.
	 * */
	public void executeBlancoAlgorithmMultipleTimes() {
		try {
			//get the list of query
			String queryPath = map.get("query.path");
			Map<String, String> queryMap = PropertiesUsefulMethods.getSinglePropertyFileMap(queryPath);
			
			//setup the connection
			ConnectionHandler.createConnectionAsOwner(map.get("jdbc.connection.string"), this.getClass().getName());
			
			for(Entry<String, String> entry : queryMap.entrySet()) {
				//id of the query
				String queryId = entry.getKey();
				String query = entry.getValue();

				//prepare the execution
				this.setQueryId(queryId);
				this.setQuery(query);

				this.executeBlancoAlgorithm(true);
				
			}
			timeWriter.close();
			ConnectionHandler.closeConnectionIfOwner(this.getClass().getName());
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}


	public String getQuery() {
		return query;
	}


	public void setQuery(String query) {
		this.query = query;
	}


	public String getQueryId() {
		return queryId;
	}


	public void setQueryId(String queryId) {
		this.queryId = queryId;
	}

	/** To configure the labels signaling which phases we want to execute.
	 * */
	public void configureLabels(Map<String, String> map) {
		uriDatabasePurifierFlag = Boolean.parseBoolean( map.get("uri.database.purifier.flag") );
		rdfConvertionAlgorithmFlag = 
				Boolean.parseBoolean( map.get("rdf.convertion.algorithm.flag") );
		rdfDatabaseToTriplesTRECDocumentConvertionFlag = 
				Boolean.parseBoolean( map.get("rdf.database.to.triples.trec.document.convertion.flag") );
		trecIndexFlag = 
				Boolean.parseBoolean( map.get("trec.index.flag") );

		matchingTripleRetrievalFlag = 
				Boolean.parseBoolean( map.get("matching.triple.retrieval.flag") );
		queryGraphGenerationFlag = 
				Boolean.parseBoolean( map.get("query.graph.generation.flag") );
		answerSubgraphGenerationFlag = 
				Boolean.parseBoolean( map.get("answer.subgraph.generation.flag") );
		fromAnswerSubgraphToTRECDocumentsFlag = 
				Boolean.parseBoolean( map.get("from.answer.subgraph.to.trec.documents.flag") );
		indexerDirectoryOfTRECFilesFlag = 
				Boolean.parseBoolean( map.get("indexer.directory.of.trec.files.flag") );
		rjDocumentCreationFlag = 
				Boolean.parseBoolean( map.get("rj.document.creation.flag") );
		answerSubgraphRankingFlag = 
				Boolean.parseBoolean( map.get("answer.subgraph.ranking.flag") );
		originalMatchingTripleAlgorithmFlag = 
				Boolean.parseBoolean(map.get("original.matching.triple.algorithm.flag"));
	}

	/** In case, purification of the database.
	 * It was necessary at the beginning to be able to read the 
	 * file for LinkedMDB. */
	protected void phase1() throws IOException {

		if(uriDatabasePurifierFlag) {
			System.out.println("Start purification phase");
			Stopwatch timer = Stopwatch.createStarted();
			
			UriDatabasePurifier phase1 = new UriDatabasePurifier();
			phase1.processFile();

			System.out.print("phase 1 ended in " + timer.stop());
			timeWriter.write("phase 1 ended in " + timer);
			timeWriter.newLine();
		}

	}

	protected void phase2() throws IOException {
		if(rdfConvertionAlgorithmFlag) {
			System.out.println("Start convertion in RDF triplestore and RDB phase (phase 2)");
			Stopwatch timer = Stopwatch.createStarted();
			
			//transformation of a RDF file in triple store and in relational database
			RDFConvertionAlgorithmsToolkit phase2 = new RDFConvertionAlgorithmsToolkit();
//			phase2.fromTextFileToRDFTripleStorePoweredByBlazegraph();
			phase2.fromRDFTripleStoreToRDBTripleStorePoweredByBlazegraph();

			System.out.println("phase 2 ended in " + timer);
			
			timeWriter.write("phase 2 ended in " + timer);
			timeWriter.newLine();
		}
	}

	protected void phase3() throws IOException {
		//Conversion of the triples in TREC documents
		if(rdfDatabaseToTriplesTRECDocumentConvertionFlag) {
			System.out.println("Start convertion of triples in TREC documents (phase 3)");
			Stopwatch timer = Stopwatch.createStarted();
			
			RDFDatabaseToTriplesTRECDocumentsConvertionPhase phase3 = new RDFDatabaseToTriplesTRECDocumentsConvertionPhase();
			phase3.setOutputDirectory(this.mainAlgorithmDirectory + "/triples_collection");
			
			phase3.rdfToTRECConverter();

			System.out.println("phase 3 ended in " + timer.stop());
			
			timeWriter.write("phase 3 ended in " + timer);
			timeWriter.newLine();
			timeWriter.flush();
		}
	}

	protected void phase4() throws IOException {
		if( trecIndexFlag ) {
			System.out.println("Start indexing phase (phase 4)");
			Stopwatch timer = Stopwatch.createStarted();
			
			//indexing of the triples collection
			TrecIndexPhase phase4 = new TrecIndexPhase();
			phase4.setCollectionDirectory(this.mainAlgorithmDirectory + "/triples_collection");
			phase4.setOutputTrecIndexPath(this.mainAlgorithmDirectory + "/triples_index");
			
			phase4.trecIndexPhase();

			System.out.println("phase 4 ended in " + timer.stop());
			
			timeWriter.write("phase 4 ended in " + timer);
			timeWriter.newLine();
			timeWriter.flush();
			
		}
	}

	protected void phase5(boolean multipleTimes) throws IOException {
		if( matchingTripleRetrievalFlag ) {
			System.out.println("Start phase to find the matching triples (phase 5)");
			if(singleMethodTimer.isRunning())
				singleMethodTimer.reset();
			Stopwatch timer = Stopwatch.createStarted();
			
			//find the triples that match the keywords of the query
			MatchingTripleRetrievalPhase phase5 = new MatchingTripleRetrievalPhase();

			if(multipleTimes) {
				phase5.setQuery(this.query);
				phase5.setQueryId(queryId);
				phase5.setOutputDirectory(mainQueryDirectory + "/" + this.queryId); 
				phase5.setIndexPath(this.mainAlgorithmDirectory + "/triples_index");
			}

			if(originalMatchingTripleAlgorithmFlag) {
				phase5.findMatchingTriples();
			} else {
				phase5.findMatchingTriplesWithPruning();
			}

			System.out.println("phase 5 ended in " + timer.stop());
			
			timeWriter.write("phase 5 ended in " + timer);
			timeWriter.newLine();
			timeWriter.flush();
		}
	}

	protected void phase6(boolean multipleTimes) throws IOException {
		if ( queryGraphGenerationFlag ) {
			System.out.println("Start generation of the query graph (phase 6)");
			if(singleMethodTimer.isRunning())
				singleMethodTimer.reset();
			Stopwatch timer = Stopwatch.createStarted();
			
			//creation of the query graph using the matching triples inside the RDB
			QueryGraphGenerationPhase phase6 = new QueryGraphGenerationPhase();
			if(multipleTimes) {
				phase6.setResultSourceFilePath(mainQueryDirectory + "/" + queryId + "/matching_triples.txt");
			}

			phase6.createQueryGraphInDatabase();

			System.out.println("phase 6 ended in " + timer.stop());
			
			timeWriter.write("phase 6 ended in " + timer);
			timeWriter.newLine();
			timeWriter.flush();
		}
	}

	protected void phase7(boolean multipleTimes) throws IOException {
		if( answerSubgraphGenerationFlag ) {
			System.out.println("Start generation of the answer graphs (phase 7)");
			if(singleMethodTimer.isRunning())
				singleMethodTimer.reset();
			Stopwatch timer = Stopwatch.createStarted();
			
			//create the answer graphs from the subgraph
			AnswerSubgraphGenerationPhase phase7 = new AnswerSubgraphGenerationPhase();
			if(multipleTimes) {
				phase7.setQuery(query);
				phase7.setQueryDirectory(mainQueryDirectory + "/" + queryId); 
			}

			phase7.generationOfAnswerSubgraphs();

			System.out.println("phase 7 ended in " + timer.stop());
			
			timeWriter.write("phase 7 ended in " + timer);
			timeWriter.newLine();
			timeWriter.flush();
			
		}
	}

	protected void phase8(boolean multipleTimes) throws IOException {
		if( fromAnswerSubgraphToTRECDocumentsFlag ) {
			System.out.println("Start convertion from answer graph to TREC answer documents (phase 8)");
			if(singleMethodTimer.isRunning())
				singleMethodTimer.reset();
			Stopwatch timer = Stopwatch.createStarted();

			//Conversion of the answer subgraphs in TREC documents
			FromAnswerSubgraphToTrecDocumentsPhase phase8 = new FromAnswerSubgraphToTrecDocumentsPhase();
			if(multipleTimes) {
				phase8.setAnswerSubgraphsMainDirectory(mainQueryDirectory + "/" + queryId + "/answer_subgraphs");
				phase8.setOutputDirectory(mainQueryDirectory + "/" + queryId + "/answer_subgraphs_collection");
			}

			phase8.convertRDFAnswerSubgraphsInTRECDocuments();

			System.out.println("phase 8 ended in " + timer.stop());
			
			timeWriter.write("phase 8 ended in " + timer);
			timeWriter.newLine();
			timeWriter.flush();
		}

	}

	protected void phase9(boolean multipleTimes) throws IOException {
		if( indexerDirectoryOfTRECFilesFlag ) {
			System.out.println("Start indexing of the answer graph (phase 9)");
			if(singleMethodTimer.isRunning())
				singleMethodTimer.reset();
			Stopwatch timer = Stopwatch.createStarted();
			
			//index the answer collection
			IndexerDirectoryOfTRECFiles phase9 = new IndexerDirectoryOfTRECFiles();
			if(multipleTimes) {
				phase9.setDirectoryToIndex(mainQueryDirectory + "/" + queryId + "/answer_subgraphs_collection");
				phase9.setIndexPath(mainQueryDirectory + "/" + queryId + "/answer_graphs_index");
			}

			phase9.index("unigram");

			System.out.println("phase 9 ended in " + timer.stop());
			
			timeWriter.write("phase 9 ended in " + timer);
			timeWriter.newLine();
			timeWriter.flush();
		}
	}

	protected void phase10(boolean multipleTimes) throws IOException {
		if( rjDocumentCreationFlag ) {
			System.out.println("Start creation of documents R_j and their indexes (phase 10)");
			if(singleMethodTimer.isRunning())
				singleMethodTimer.reset();
			Stopwatch timer = Stopwatch.createStarted();
			
			//creation of the R_j collection (documents and indexes)
			RjDocumentCreationPhase phase10 = new RjDocumentCreationPhase();
			if(multipleTimes) {
				phase10.setAnswerSubgraphsDirectory(mainQueryDirectory + "/" + queryId + "/answer_subgraphs");
				phase10.setRjOutputDirectory(mainQueryDirectory + "/" + queryId + "/rj");
			}

			phase10.createTheRjFiles();

			System.out.println("phase 10 ended in " + timer.stop());
			
			timeWriter.write("phase 10 ended in " + timer);
			timeWriter.newLine();
			timeWriter.flush();
			
		}
	}

	protected void phase11(boolean multipleTimes) throws IOException {
		if( answerSubgraphRankingFlag ) {
			System.out.println("Start ranking of the answers (phase 11)");
			if(singleMethodTimer.isRunning())
				singleMethodTimer.reset();
			Stopwatch timer = Stopwatch.createStarted();
			
			//do the ranking
			AnswerSubgraphsRankingPhase phase11 = new AnswerSubgraphsRankingPhase();
			if(multipleTimes) {
				phase11.setQuery(query);
				phase11.setResultDirectory(mainQueryDirectory + "/" + queryId + "/rank");
				phase11.setIndexPath(mainQueryDirectory + "/" + queryId + "/answer_graphs_index"); 

				//set properties for the Blanco-Elbassuoni Language Model 
				System.setProperty("blanco.rj.indexes.directory", mainQueryDirectory + "/" + queryId + "/rj_indexes");
				System.setProperty("blanco.rj.index.directory", mainQueryDirectory + "/" + queryId + "/rj_index");
				System.setProperty("blanco.r.j.output.directory", mainQueryDirectory + "/" + queryId + "/rj");
				System.setProperty("blanco.subgraphs.directory", mainQueryDirectory + "/" +queryId + "/answer_subgraphs");
				System.setProperty("answer.index.directory", mainQueryDirectory + "/" + queryId + "/answer_graphs_index");
			}

			phase11.subgraphsRankingPhase();

			System.out.println("phase 11 ended in " + timer.stop());
			
			timeWriter.write("phase 11 ended in " + timer);
			timeWriter.newLine();
			timeWriter.flush();
		}
	}
	
	
	/**test main*/
	public static void test(String[] args) throws IOException {
		BlancoMainClass execution = new BlancoMainClass();
		if(execution.multipleTimesFlag) {
			execution.executeBlancoAlgorithmMultipleTimes();
		} else {
			execution.executeBlancoAlgorithm();
		}
	}

	public Connection getConnection() {
		return connection;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}

}
