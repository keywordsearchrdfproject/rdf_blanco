package it.unipd.dei.ims.blanco.algorithm.main;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.jena.ext.com.google.common.base.Stopwatch;

import it.unipd.dei.ims.datastructure.ConnectionHandler;
import it.unipd.dei.ims.datastructure.ThreadState;
import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;

public class SLMControlRoomWithTimeouts extends BlancoMainClass {

	private boolean isOffline = true;

	/** provided time for the off-line phase in hours*/
	private int offLineTime;

	/** provided time for the on-line time phase in minutes*/
	private int onLineTime;

	public SLMControlRoomWithTimeouts() {
		super();

		this.isOffline = Boolean.parseBoolean(map.get("is.offline"));

		String time = map.get("on.line.time");
		time = (time != null) ? time : "1000";
		this.onLineTime = Integer.parseInt(time);

		time = map.get("off.line.time");
		time = (time != null) ? time : "48";
		this.offLineTime = Integer.parseInt(time);
	}


	protected void offLinePhaseWithTimer() {
		ThreadState.setOffLine(true);
		ExecutorService executor = Executors.newSingleThreadExecutor();
		Future<String> future = null;

		try {
			//take the connection once and for all
			ConnectionHandler.createConnectionAsOwner(
					map.get("jdbc.connection.string"), 
					this.getClass().getName());

			future = executor.submit(new OfflineTask(this));

			String s = future.get(this.offLineTime, TimeUnit.HOURS);
			System.out.println(s);
			timeWriter.write(s);
			timeWriter.newLine();
			timeWriter.flush();
			timeWriter.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			future.cancel(true);
			ThreadState.setOffLine(false);
			System.out.println("there was an InterruptedException during the off-line"
					+ " phase of SLM");
			e.printStackTrace();
		} catch (ExecutionException e) {
			future.cancel(true);
			ThreadState.setOffLine(false);
			System.out.println("there was an ExecutionException during the off-line phase of SLM");
			e.printStackTrace();
		} catch (TimeoutException e) {
			future.cancel(true);
			ThreadState.setOffLine(false);
			System.out.println("the off-line phase of SLM could not terminate in time!");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("IO exeption writing log files in offline phase of SLM");
			e.printStackTrace();
		} finally {
			try {
				ConnectionHandler.closeConnectionIfOwner(this.getClass().getName());
			} catch (SQLException e) {
				System.err.println("error in closing the connection at the end of the off-line phase");
				e.printStackTrace();
			}
		}
		executor.shutdownNow();
	}

	/**Thread that deals with the off-line phase
	 * of SLM.
	 * */
	private class OfflineTask implements Callable<String> {
		private SLMControlRoomWithTimeouts execution;


		public OfflineTask(SLMControlRoomWithTimeouts exec) {
			this.execution = exec;
		}

		@Override
		public String call() throws Exception {
			System.out.println("Starting the execution of the off-line "
					+ "phase of SLM");

			Stopwatch offFlineTimer = Stopwatch.createStarted();
			//simply call the offlinePhase() method that we have in the MainClass
			this.execution.offlinePhase();
			return "TIME off-line " + offFlineTimer.stop();
		}
	}

	/** This method is called from the OfflineTask 
	 * to perform the methods of the superclass BlancoMainClass
	 * that perform the off-line phase of SLM.
	 * */
	private void offlinePhase() {
		try {
			if(ThreadState.isOffLine())
				this.phase1();//purification, often not necessary
			if(ThreadState.isOffLine())
				this.phase2();//transformation from text to RDF store, often not necessary
			if(ThreadState.isOffLine())
				this.phase3();//create single triple documents using PostgreSQL tables
			if(ThreadState.isOffLine())
				this.phase4();//indexing
		} catch (IOException e) {
			System.err.println("IOException during the off-line phase of SLM");
			e.printStackTrace();
		}
	}

	/** Executes the on-line phase of the algorithm
	 * once for every query in the query file
	 * specified in the query.file property
	 * in the main.properties file.
	 * 
	 * */
	protected void onlinePhaseWithTimer() {
		//get the list of queries
		String queryPath = this.map.get("query.path");

		try {
			//read the queries from the file
			Map<String, String> queryMap = 
					PropertiesUsefulMethods.getSinglePropertyFileMap(queryPath);
			//open the connection to the SQL database
			ConnectionHandler.createConnectionAsOwner(
					map.get("jdbc.connection.string"), 
					this.getClass().getName());

			for(Entry<String, String> entry : queryMap.entrySet()) {
				//for each query

				//we signal that we are authorized to perform this query 
				ThreadState.setOnLine(true);

				//set the information of this query
				String queryId = entry.getKey();
				String query = entry.getValue();
				this.setQueryId(queryId);
				this.setQuery(query);

				this.executeForOneQueryWithTimer();
			}
			timeWriter.close();
		} catch (IOException e) {
			System.err.println("errors during the readings of the queries in the file: " + queryPath);
			e.printStackTrace();
		} catch (SQLException e) {
			System.err.println("Error during the establishment of a connection to the database");
			e.printStackTrace();
		} finally {
			try {
				ConnectionHandler.closeConnectionIfOwner(this.getClass().getName());
			} catch (SQLException e) {
				System.err.println("error closing the connection to the database");
				e.printStackTrace();
			}
		}

	}

	/** This method executes one query at a time, 
	 * one on-line phase per time. It makes sure that every phase is 
	 * executed inside a maximum time of 1000s or the 
	 * ones provided in the on.line property
	 * */
	private void executeForOneQueryWithTimer() {
		//create the thread that will take care of this query
		ExecutorService executor = Executors.newSingleThreadExecutor();
		Future<String> future = executor.submit(new SLMOnlinePhaseTask(this));
		
		try {
			//execution of the query
			String s = future.get(this.onLineTime, TimeUnit.SECONDS);
			System.out.println(s);
			timeWriter.write(s);
		} catch (InterruptedException e) {
			future.cancel(true);
			ThreadState.setOnLine(false);
			System.out.println("there was an InterruptedException during SLM at "+ this.queryId);
			e.printStackTrace();
		} catch (ExecutionException e) {
			future.cancel(true);
			ThreadState.setOnLine(false);
			System.out.println("there was an ExecutionException during SLM at query " + this.queryId);
			e.printStackTrace();
		} catch (TimeoutException e) {
			future.cancel(true);
			ThreadState.setOnLine(false);
			System.out.println("The query " + this.queryId + " could not end due to TimeoutException");
			System.out.println("TIME MRF-KS " + this.queryId + " -1");//in this case, we have 1000 seconds
			e.printStackTrace();
		} catch (IOException e) {
			future.cancel(true);
			ThreadState.setOnLine(false);
			System.err.println("error with the writer in SLM at query " + this.queryId);
			e.printStackTrace();
		}
		executor.shutdownNow();
	}

	/** A class that performs the execution of one query. This is a thread that we use
	 * in the rest of the class in order to be able to interrupt it
	 * after a certain amount of time.
	 * */
	private class SLMOnlinePhaseTask implements Callable<String> {
		/**link to the current execution*/
		private SLMControlRoomWithTimeouts execution;

		/**Constructor to get the reference to our current execution
		 * with all the data we need.
		 * */
		public SLMOnlinePhaseTask(SLMControlRoomWithTimeouts exec) {
			this.execution = exec;
		}

		@Override
		public String call() throws Exception {
			System.out.println("starting the execution of the MRF-KS pipeline for query " + this.execution.queryId);
			Stopwatch timer = Stopwatch.createStarted();

			/*the boolean true is a legacy from previous
			 * implementations. It is necessary in order
			 * for the method to set the new query and queryId
			 * */
			this.execution.onLinePhase(true);//simply call the method of the parent class
			return "TIME SLM " + this.execution.queryId + " " + timer.stop();
		}
	}

	private void onLinePhase(boolean multipleTimes) {
		try {
			if(ThreadState.isOnLine())
				this.phase5(multipleTimes);
			if(ThreadState.isOnLine())
				this.phase6(multipleTimes);
			if(ThreadState.isOnLine())
				this.phase7(multipleTimes);
			if(ThreadState.isOnLine())
				this.phase8(multipleTimes);
			if(ThreadState.isOnLine())
				this.phase9(multipleTimes);
			if(ThreadState.isOnLine())
				this.phase10(multipleTimes);
			if(ThreadState.isOnLine())
				this.phase11(multipleTimes);
		} catch (IOException e) {
			System.err.println("IOException during query number " + this.queryId);
			e.printStackTrace();
		}
	}




	public static void main(String[] args) {
		SLMControlRoomWithTimeouts execution = new SLMControlRoomWithTimeouts();

		if(execution.isOffline) {
			execution.offLinePhaseWithTimer();
			System.exit(0);
		} else {
			execution.onlinePhaseWithTimer();
		}

	}

}
