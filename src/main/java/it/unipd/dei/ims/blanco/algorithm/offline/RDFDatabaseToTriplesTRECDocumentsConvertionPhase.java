package it.unipd.dei.ims.blanco.algorithm.offline;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import it.unipd.dei.ims.datastructure.ConnectionHandler;
import it.unipd.dei.ims.datastructure.DatabaseState;
import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;
import it.unipd.dei.ims.rum.utilities.SQLUtilities;
import it.unipd.dei.ims.rum.utilities.StringUsefulMethods;
import it.unipd.dei.ims.rum.utilities.UrlUtilities;
import it.unipd.dei.ims.rum.utilities.UsefulConstants;

/** Blanco algorithm: phase 1. 
 * <p>.
 * OFFLINE: this phase needs to be done only once per graph. The information and
 * data created with this phase are query-independent
 * <p>
 * Given an RDF graph saved as triple store in an RDB
 * it converts its triples in trec documents.
 * That is, it creates multiple file trec containing multiple
 * documents. Each of these documents represent one triple.
 * 
 * */
public class RDFDatabaseToTriplesTRECDocumentsConvertionPhase {
	/** String to use in order to connect to the PostgreSQL database.
	 * Property jdbc.connection.string in convertion.properties
	 * */
	protected String jdbcConnectingString;

	/** Directory where to save the TREC files containing the
	 * documents corresponding to the triples.
	 * */
	protected String outputDirectory;

	private static final String SQL_SELECT_TRIPLES = 
			"SELECT id_, subject_, predicate_, object_" + 
			"	FROM " + DatabaseState.getSchema() + ".triple_store "
					+ "order by id_ ASC LIMIT ? OFFSET ?;";

	public RDFDatabaseToTriplesTRECDocumentsConvertionPhase() {

		try {
			this.setup();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/** To invoke to prepare the elements of this class.
	 * @throws IOException 
	 * */
	public void setup() throws IOException {
		Map<String, String> map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/main.properties");
		//set the string to connect to the Postgre database
		this.jdbcConnectingString = map.get("jdbc.connection.string");
		//output directory
		this.outputDirectory = map.get("output.trec.collection.directory");
		
		

	}


	/** Method to convert a database saved in a relational database into 
	 * TREC documents collected inside files.
	 * */
	public void rdfToTRECConverter() {
		//connect to the relational database
		Connection connection = null;

		//clean the directory, because we don't want files mixed with other databases
		//XXX
		try {
			File f = new File(outputDirectory);
			if(!f.exists()) {
				f.mkdirs();
			}
			FileUtils.cleanDirectory(new File(outputDirectory));
		} catch (IOException e1) {
			e1.printStackTrace();
			System.err.println("unable to clean the directory");
		}
		//directory is now clean

		//limit and offset for the select query
		int limit = 131072;//power of 2. Because it is always a good choice
		//XXX
		int offset = 0;//usually to 0. If different, because we are integrating

		int counter = 0;
		//XXX
		//counter for the name of the TREC files
		int fileCounter = 0;

		try {
			//connect to the database
			connection = ConnectionHandler.createConnectionAsOwner(this.jdbcConnectingString, this.getClass().getName());

			//read all the triples one by one
			while(true) {
				//get the iterator over the triples
				ResultSet iterator = SQLUtilities.executeOffsetQuery(connection, limit, offset, SQL_SELECT_TRIPLES);
				//update the offset
				offset = offset + limit;
				//check we still have something to read
				if(iterator.first()) {
					//create a new file
					String outputFile = outputDirectory + "/" + (++fileCounter) + ".txt";
					//create the reader to our output file
					Path outputPath = Paths.get(outputFile);
					BufferedWriter writer = Files.newBufferedWriter(outputPath, UsefulConstants.CHARSET_ENCODING); 

					//get the cursor to the begin
					iterator.beforeFirst();
					while(iterator.next()) {
						writeOneDocument(iterator, writer);
						counter++;
					}//written a block of triples
					writer.flush();
					writer.close();
					System.out.println("written " + counter + " triples");
				} else {
					//we have read all the triples, we can exit
					break;
				}
			} //end of while(true). Read all of the triples
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(connection!=null) {
				try {
					ConnectionHandler.closeConnectionIfOwner(this.getClass().getName());
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/** Using the provided BufferedWriter, writes one document inside the TREC file.
	 * 
	 *  @param rs The result set pointed in the triple that we are writing.
	 * @param writer BufferedWriter to the file that we are writing.
	 * */
	private static void writeOneDocument(ResultSet rs, BufferedWriter writer) throws SQLException, IOException {
		//read the next triple
		int id_ = rs.getInt(1);

		writer.write("<DOC>");
		writer.newLine();

		writer.write("<DOCNO>" + id_ + "</DOCNO>");
		writer.newLine();

		writeTheTriple(rs, writer);

		writer.write("</DOC>");
		writer.newLine();
	}

	/** Converts one triple in a string and print is using the provided writer.
	 * 
	 * @param rs The result set pointed in the triple that we are writing.
	 * @param writer BufferedWriter to the file that we are writing.
	 * */
	private static void writeTheTriple(ResultSet rs, BufferedWriter writer) throws SQLException, IOException {
		String subject_ = rs.getString(2);
		String predicate_ = rs.getString(3);
		String object_ = rs.getString(4);

		//do the subject
		String s = UrlUtilities.takeWordsFromIri(subject_);
		writer.write(s + " ");

		//do the predicate
		s = UrlUtilities.takeWordsFromIri(predicate_);
		writer.write(s + " ");

		//do the object
		if(UrlUtilities.checkIfValidURL(object_)) {
			s = UrlUtilities.takeWordsFromIri(object_);
			writer.write(s + " ");
		} else {
			s = StringUsefulMethods.getFirstPartOfRDFLiteral(object_);
			writer.write(s);
		}
		writer.newLine();

	}
	
	/** Test main */
	public static void main(String[] args) {
		RDFDatabaseToTriplesTRECDocumentsConvertionPhase phase = 
				new RDFDatabaseToTriplesTRECDocumentsConvertionPhase();
		phase.rdfToTRECConverter();
	}

	public String getOutputDirectory() {
		return outputDirectory;
	}

	public void setOutputDirectory(String outputDirectory) {
		this.outputDirectory = outputDirectory;
	}

	public String getJdbcConnectingString() {
		return jdbcConnectingString;
	}

	public void setJdbcConnectingString(String jdbcConnectingString) {
		this.jdbcConnectingString = jdbcConnectingString;
	}
}
