package it.unipd.dei.ims.blanco.algorithm.offline;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.terrier.indexing.Collection;
import org.terrier.indexing.TRECCollection;
import org.terrier.structures.indexing.Indexer;
import org.terrier.structures.indexing.classical.BasicIndexer;

import it.unipd.dei.ims.rum.utilities.PathUsefulMethods;
import it.unipd.dei.ims.terrier.utilities.PropertiesUsefulMethods;

/**Blanco algorithm: phase 2.
 * <p>
 * OFFLINE (you should do this only once per dataset)
 * <p>
 * The indexing of the TREC files in an index.
 * This class indexes trec files whose path are containet in
 * a collection.etc file. It is more efficient to index via batch, 
 * nonetheless.
 * <p>
 * Property file: properties/trec_index_phase.properties
 * */
@SuppressWarnings("unused")
public class TrecIndexPhase {
	
	/** The file collection.etc containing the paths of the files .TREC we need to index.
	 * In this class we use this file in order to collect the names of the files because we suppose
	 * there is a big number of them.
	 * <p>
	 * You should prepare this file with the 
	 * <xmp>
	 * sh trec_setup.sh [path of the directory with the files]
	 * </xmp>
	 * command.
	 * <p>
	 * property collection.filename
	 * */
	@Deprecated
	private String collectionFilename;
	
	/** Path of the directory where to put the output index obtained from the collection.
	 * <p>
	 * Property output.trec.index.path
	 * */
	private String outputTrecIndexPath;
	
	/** Directory where the TREC collection is stored*/
	private String collectionDirectory;
	

	public TrecIndexPhase() {
		try {
			this.setup();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void setup() throws IOException {
		Map<String, String> map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/main.properties");
		String terrierHome = map.get("terrier.home");
		String terrierEtc = map.get("terrier.etc");

		//setting system properties for terrier to find the terrier_home and the /etc directory
		System.setProperty("terrier.home", terrierHome);
		System.setProperty("terrier.etc", terrierEtc);
		
		//deprecated
		collectionFilename = map.get("collection.filename");
		outputTrecIndexPath = map.get("output.trec.index.path");
		
		
		collectionDirectory = map.get("collection.directory");
	}
	
	/** Indexes the TREC collection using the properties collection.filename to read the file .etc
	 * with the list of file to index and output.trec.index.path to know the path of the directory where to write them.
	 * */
	public void trecIndexPhase() {
		File f = new File(outputTrecIndexPath);
		if(!f.exists()) {
			f.mkdirs();
		}
		
		List<String> list = PathUsefulMethods.getListOfFiles(collectionDirectory);
		Collection coll = new TRECCollection(list);
		
		//index the collection
		//indexing the collection
		Indexer indexer = new BasicIndexer(this.outputTrecIndexPath, "data");
		indexer.index(new Collection[]{ coll });
	}

	public String getOutputTrecIndexPath() {
		return outputTrecIndexPath;
	}

	public void setOutputTrecIndexPath(String outputTrecIndexPath) {
		this.outputTrecIndexPath = outputTrecIndexPath;
	}

	public String getCollectionDirectory() {
		return collectionDirectory;
	}

	public void setCollectionDirectory(String collectionDirectory) {
		this.collectionDirectory = collectionDirectory;
	}
}
