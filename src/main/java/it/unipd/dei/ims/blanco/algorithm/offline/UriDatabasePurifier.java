package it.unipd.dei.ims.blanco.algorithm.offline;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.jena.ext.com.google.common.base.Stopwatch;

import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;
import it.unipd.dei.ims.rum.utilities.UsefulConstants;

/** Blanco algorithm, phase 0.1.
 * <p>
 * This class was born due to the fact that LinkedMDB presented some url
 * that were incorrect and could not be read by RDF libraries like Blazegraph and Jena. 
 * Implements a method to re-write the database with correct url implementing some corrections.
 * <p>
 * This class was only tested on the LinkedMDB file, the only one that required this kind of preprocessing.
 * 
 * */
public class UriDatabasePurifier {

	/** Timer to check the time required for the computation */
	private Stopwatch timer;

	/** path of the RDF file to be read and corrected */
	private String inputFilePath;
	/** Path of the output RDF file */
	private String outputFilePath;

	/** List of characters to be corrected and the string which needs to be used in order to correct them.
	 * */
	private List<Pair<String, String>> convertingList;

	public UriDatabasePurifier() {
		//the timer, created but stopped
		timer = Stopwatch.createUnstarted();
		
		convertingList = new ArrayList<Pair<String, String>>();
		
		try {
			this.setup();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/** To invoke in order to setup all the variables inside the program reading 
	 * from the properties files.
	 * <p>
	 * reads from: properties/path.properties
	 * <ul>
	 * <li>input.file.path: path of a graph in format .nt to be read and checked.</li>
	 * <li>output.file.path: path where to write the output RDF graph corrected.
	 * </ul>
	 * @throws IOException 
	 * */
	private void setup() throws IOException {
		Map<String, String> map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/main.properties");

		this.inputFilePath = map.get("input.file.path");
		this.outputFilePath = map.get("output.file.path");

		//take the information about the char to change
		this.retrieveCharactersToBeChanged();
		System.out.println("Setup completed...");
	}

	/** retrieves from the character.properties file the list of characters to be 
	 * changed and the elements to use in order to change them.
	 * <p>
	 * As a convention, we use the value of the property as information. An example is
	 * <xmp>
	 * char1=\\s+_%20

		char2="_%22

		char3=\\{_%7B

		char4=\\}_%7D

		char5=`_%60
	 * </xmp>
	 * The underscore _ is used to separate the simbol to be converted in the string that
	 * will be put in its place. 
	 * */
	private void retrieveCharactersToBeChanged() {
		InputStream input = null;
		Properties prop = new Properties();

		try {
			//red the property file
			input = new FileInputStream("properties/characters.properties");
			prop.load(input);
			Pair<String, String> par = null;

			//take all the elements
			Enumeration<?> enumeration = prop.propertyNames();
			while(enumeration.hasMoreElements()) {
				//iterate 
				String key = (String) enumeration.nextElement();
				//get the value
				String value = prop.getProperty(key);
				//get the information
				String[] couple = value.split("_");
				par = Pair.of(couple[0], couple[1]);
				//insert into the string
				this.convertingList.add(par);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public void processFile() {
		//Paths to read and write
		Path inputPath = Paths.get(this.inputFilePath);
		Path outputPath = Paths.get(this.outputFilePath);

		//we use an id to identify each triple in the graph
		int lineId = 0;
		timer.start();

		//open the reader and the writer
		try(BufferedReader reader = Files.newBufferedReader(inputPath, UsefulConstants.CHARSET_ENCODING);
				BufferedWriter writer = Files.newBufferedWriter(outputPath, UsefulConstants.CHARSET_ENCODING);) {
			String line = null;
			while ((line = reader.readLine()) != null) {
				//correct the line
				line = purifyLine(line);
				//write it down
				writer.write(line + " #" + lineId++);
				writer.newLine();
			}      
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.print("Correction of the RDF file completed in " + timer.stop());
	}

	/** Executes the correction of a line of RDF file. We
	 * consider a line containing exactly one triple in .nt format.
	 * */
	private String purifyLine(String originalLine) {
		String purifiedString = "";
		String work = "";

		//first of all, split the strings
		//see https://www.w3.org/TR/n-triples/ for clarifications
		//(not necessarily the definitive RE, in future may need to change)
		String patternString = "<(.*?)>|.$|\"(.*)\"(\\^\\^<(.*?)>)?";

		Pattern pattern = Pattern.compile(patternString);
		Matcher matcher = pattern.matcher(originalLine);

		while(matcher.find()) {
			//take the match
			work = matcher.group();
			//understand if it is an url (it begins with '<') or a literal
			char firstChar = work.charAt(0);
			//check the wrong characters
			if(firstChar == '<')//it is a IRI
			{
				for(Pair<String, String> pair: this.convertingList) {
					String left = pair.getLeft();
					String right = pair.getRight();
					work = work.replaceAll(left, right);
				}

			}
			
			if(work.equals("."))
				purifiedString = purifiedString + work;
			else
				purifiedString = purifiedString + work + " ";
		}

		return purifiedString;

	}
}
