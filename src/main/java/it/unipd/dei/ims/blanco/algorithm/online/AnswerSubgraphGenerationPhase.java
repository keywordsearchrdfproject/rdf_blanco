package it.unipd.dei.ims.blanco.algorithm.online;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.Value;
import org.openrdf.model.impl.StatementImpl;
import org.openrdf.model.impl.TreeModel;
import org.openrdf.model.impl.URIImpl;

import it.unipd.dei.ims.datastructure.ConnectionHandler;
import it.unipd.dei.ims.datastructure.DatabaseState;
import it.unipd.dei.ims.datastructure.ThreadState;
import it.unipd.dei.ims.rum.utilities.BlancoUsefuMethods;
import it.unipd.dei.ims.rum.utilities.BlazegraphUsefulMethods;
import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;
import it.unipd.dei.ims.rum.utilities.UrlUtilities;
import it.unipd.dei.ims.terrier.utilities.TerrierUsefulMethods;

/** Blanco algorithm: phase 7
 * <p>
 * 
 * This phase utilizes the query graph E to build the different proposed subgraphs.
 * <p>
 * property file: properties/trec_subgraph_generation_phase.properties
 * */
public class AnswerSubgraphGenerationPhase {

	private static final String SQL_SELECT_QUERY_GRAPH_TRIPLES = "SELECT id_, subject_, "
			+ "predicate_, object_ from "
			+ DatabaseState.getSchema() + ".query_graph order by id_ "
			+ "limit ? offset ?";

	private int numberOfTriple;
	private int numberOfGraphs;
	private int directoryCounter;
	
	private String jdbcConnectionString;
	
	private String queryDirectory;
	
	private String query;
	
	/** Flag to indicate if we use the original condition
	 * expressed in Blanco Elbassuoni 2010 or a more lenient
	 * one.
	 * */
	private boolean originalAddingTripleConditionFlag;

	public AnswerSubgraphGenerationPhase () {
		try {
			numberOfTriple = 0;
			numberOfGraphs = 0;
			directoryCounter = 0;
			this.setup();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void setup() throws IOException {
		Map<String, String> map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/main.properties");
		String terrierHome = map.get("terrier.home");
		String terrierEtc = map.get("terrier.etc");
		System.setProperty("terrier.home", terrierHome);
		System.setProperty("terrier.etc", terrierEtc);

		jdbcConnectionString = map.get("jdbc.connection.string");
		queryDirectory = map.get("query.directory");
		query = map.get("query");
		this.originalAddingTripleConditionFlag = Boolean.parseBoolean(map.get("original.adding.triple.condition.flag"));
	}
	
	/** Generates, provided a base graph E, the corresponding subgraphs.
	 * 
	 * @param jdbcConnectingString jdbc connection string to the support database
	 * @param queryDirectoryPath path of the directory containing all the data about the query
	 * @param query the query in keywords we are using to generate the subgraphs
	 * */
	public void generationOfAnswerSubgraphs () {
		Connection connection = null;
		try {
			String subgraphsDirectory = this.queryDirectory + "/answer_subgraphs";
			File dir = new File(subgraphsDirectory);
			if(!dir.exists()) {
				dir.mkdirs();
			}
			
			//clear the directory of all the older subgraphs
			//obviously in the tests I won't take in consideration the time of 
			//the cleaning procedure
			FileUtils.cleanDirectory(dir);
			
			//connect to the Relational Database
			connection = ConnectionHandler.createConnectionAsOwner(jdbcConnectionString, this.getClass().getName());
			
			int limit = 500;
			int offset = 0;
			//list of the keywords
			List<String> keywords = TerrierUsefulMethods.getDocumentWordsWithTerrierAsList(query);
			
			//list to keep track of the graphs already produced and avoid the generation of subgraphs of subgraphs
			List<List<Integer>> subGraphList = new ArrayList<List<Integer>>();
			
			
			while(true) {
				//list all the triples in their id_ order
				PreparedStatement ps = connection.prepareStatement(SQL_SELECT_QUERY_GRAPH_TRIPLES, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
				ps.setInt(1, limit);
				ps.setInt(2, offset);
				
				ResultSet rs = ps.executeQuery();
				if(rs.next()) {
					rs.absolute(0);
					while(rs.next()) {
						//for each triple of the E graph
						int id_ = rs.getInt(1);
						//try to expand the subgraph starting from the seed triple
						++numberOfTriple;
						
						if(Thread.interrupted()) {
							ThreadState.setOnLine(false);
							return;
						}
						
						this.expandSubgraph(connection, id_, subGraphList, this.queryDirectory, keywords);
					}
				}
				else
					break;
				//update the offset
				offset = offset + limit;
			}
			ConnectionHandler.closeConnectionIfOwner(this.getClass().getName());
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
			finally {
		
		}
	}
	
	/**  The Blanco algorithm to expand a subgraph from the query graph E.
	 * <p>
	 * The graphs need to be unique and maximal. During the construction of a subgraph,
	 * we add only the triples with an id greater of the one of the triple. In this way we ensure
	 * the uniqueness of the graphs.
	 * <p>
	 * Also, we have to guarantee that all the graphs are maximal. To do so, we check every time
	 * that we produce a graph that it's not a subgraph of another graph already produced.
	 * 
	 * @param connection the connection to the database where to find the query graph E.
	 * @param id_ the id of the starting triple.
	 * @param keywords a list of keywords of the query to be used for the stopping condition of the algorithm
	 * 
	 * @throws SQLException 
	 * */
	private void expandSubgraph(Connection connection, int id_, 
			List<List<Integer>> duplicateList, String queryDirectoryPath,
			List<String> keywords) throws SQLException {

		//create the model
		org.openrdf.model.Model model = new TreeModel();

		//a list to contain the words of the graph, in order to know when to stop
//		List<String> graphWordList = new ArrayList<String>();
		List<List<String>> keywordsSets = new ArrayList<List<String>>();
		
		//set with the words inside the subgraph we are going to derive
		Set<String> subgraphWordsSet = new HashSet<String>();
		
		//list of ID to keep track of the graphs to store.
		List<Integer> idList = new ArrayList<Integer>();
		
		//one queue to keep the triples of the cloud we are exploring
		Queue<Integer> cloudQueue = new LinkedList<Integer>();
		cloudQueue.add(id_);
		
		while(!cloudQueue.isEmpty()) {
			//take the id of this triple
			int id = cloudQueue.remove();
			//extrapolate the triple with the specified id
			Statement triple = getTripleFromId(connection, id);
			
			//create a list with the words from the triple
			List<String> tripleWordList = BlancoUsefuMethods.getWordsFromStatementToList(triple);
			
			
			//extrapolate the keywords contained in the triple
			List<String> tripleKeywordList = BlancoUsefuMethods.extrapolateKeywordsFormList(tripleWordList, keywords);
			
			
			//condition to add the triple as explained in the paper
			if(originalAddingTripleConditionFlag) {
				if(!BlancoUsefuMethods.addTripleCondition(tripleKeywordList, keywordsSets))
					continue;
			} else { //relaxed condition suggested by me in order to have bigger and more significative subgraphs
				if(!BlancoUsefuMethods.addTripleConditionRelaxed(tripleWordList, subgraphWordsSet, tripleKeywordList, keywordsSets)) {
					continue;
				}
			}
			//if we are here, we can continue with the insertion of the triple
			
			//update the words contained in the subgraph
			subgraphWordsSet.addAll(tripleWordList);
			
			//add the id of this triple to the list of added triples
			idList.add(id);
			
			//add the triple to the model
			model.add(triple);
			
			//add the keyword list of the triple to the set of keywords of this graph
			keywordsSets.add(tripleKeywordList);
			
			//add the neighbors of the triple to the queue
			BlancoUsefuMethods.addNeighboursToQueue(cloudQueue, triple, connection, id);
		}
		
		//check if the graph that we have created is subgraph of any other subgraph already created
		if(subgraphCondition(duplicateList, idList)) {
			//in this case, the graph is subgraph of another graph. We don't print it
			return;
		}
		duplicateList.add(idList);
		
		numberOfGraphs++;
		//if we are here, we need to print the model in the directory
		String graphPath = queryDirectoryPath + "/answer_subgraphs";
		File dir = new File(graphPath);
		if(!dir.exists())
			dir.mkdirs();
		
		//check if it is necessary to build a new directory
		if(numberOfGraphs==1 || numberOfGraphs%2048==0) {
			directoryCounter++;
			dir = new File(graphPath + "/" + directoryCounter);
			if(!dir.exists()) {
				dir.mkdirs();
			}
			System.out.println("checked out " + (numberOfTriple) + " triples, printed " + numberOfGraphs + " graphs");
		}
		
		//complete the path of the graph
		graphPath = graphPath + "/" + directoryCounter + "/" + numberOfGraphs + ".ttl";
		BlazegraphUsefulMethods.printTheDamnGraph(model, graphPath);
		
	}
	
	/** Creates a triple statement from the id of a triple in the relational database
	 * of the connection object.
	 * */
	private static Statement getTripleFromId(Connection connection, int id_) throws SQLException {
		//get the triple from the RDB
		String sql = "SELECT id_, subject_, predicate_, object_ from "
				+ DatabaseState.getSchema() + ".query_graph where id_=?;";
		PreparedStatement ps = connection.prepareStatement(sql);
		ps.setInt(1, id_);
		ResultSet rs = ps.executeQuery();
		rs.next();

		String sbj = rs.getString("subject_");
		String pr = rs.getString("predicate_");
		String obj = rs.getString("object_");

		//create the triple statement
		URI subject = new URIImpl(sbj);
		URI predicate = new URIImpl(pr);
		Value object;
		if(UrlUtilities.checkIfValidURL(obj)) {
			//obj is a URL
			object = new URIImpl(obj);
		} else {
			object = BlazegraphUsefulMethods.dealWithTheObjectLiteralString(obj);
		}

		Statement stat = new StatementImpl(subject, predicate, object);
		return stat;
	}
	
	/** given a list of lists and a new list, checks if the new list in contained or equal to 
	 * any of the other lists. 
	 * */
	private static boolean subgraphCondition(List<List<Integer>> duplicateList, List<Integer> idList) {
		for(List<Integer> greatList : duplicateList) {
			if(greatList.containsAll(idList)) {
				return true;
			}
		}
		return false;
	}
	
	
	////
	
	/** main to test the class */
	public static void test(String[] args) {
		AnswerSubgraphGenerationPhase phase = new AnswerSubgraphGenerationPhase();
		phase.generationOfAnswerSubgraphs();
		System.out.println("done");
	}

	public String getQueryDirectory() {
		return queryDirectory;
	}

	public void setQueryDirectory(String queryDirectory) {
		this.queryDirectory = queryDirectory;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}
}
