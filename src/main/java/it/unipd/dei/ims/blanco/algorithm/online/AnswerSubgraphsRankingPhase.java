package it.unipd.dei.ims.blanco.algorithm.online;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

import org.terrier.matching.ResultSet;
import org.terrier.matching.models.BE_LM_StaticVariablesHolder;
import org.terrier.querying.Manager;
import org.terrier.querying.SearchRequest;
import org.terrier.structures.Index;
import org.terrier.structures.IndexOnDisk;

import it.unipd.dei.ims.blanco.algorithm.offline.TrecIndexPhase;
import it.unipd.dei.ims.rum.utilities.UsefulConstants;
import it.unipd.dei.ims.terrier.utilities.PropertiesUsefulMethods;

/** Phase 8 of the Blanco algorithm.
 * Once created the index of the subgraphs (you can do that via batch command
 * with Terrier or with {@link TrecIndexPhase}), we need to rank them in order
 * to return them to the user.
 * <p>
 * This class uses a custom made Language Model in order to generate this ranking.
 * */
public class AnswerSubgraphsRankingPhase {

	private String resultDirectory;
	/** Path of the directory where the index of the answer document is stored*/
	private String indexPath;
	
	private String query;
	
	public AnswerSubgraphsRankingPhase () {
		try {
			setup();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void setup() throws IOException {
		Map<String, String> map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/main.properties");

		//get the terrier home and etc directory where we have the property file and set them
		String terrierHome = map.get("terrier.home");
		String terrierEtc = map.get("terrier.etc");

		System.setProperty("terrier.home", terrierHome);
		System.setProperty("terrier.etc", terrierEtc);

		
		//set the number of results we want 
		System.setProperty("trec.output.format.length", ""+1000);
		System.setProperty("matching.retrieved_set_size", ""+1000);

		this.resultDirectory = map.get("result.directory");
		
		this.indexPath = map.get("trec.answer.index.path");
		
		query = map.get("query");
	}

	public void subgraphsRankingPhase() throws IOException {
		//open the index
		Index index = IndexOnDisk.createIndex(indexPath, "data");
		System.out.println("We have indexed " + index.getCollectionStatistics().getNumberOfDocuments() + " answer documents");

		Manager queryingManager = new Manager(index);

		//execute the query
		SearchRequest srq = queryingManager.newSearchRequestFromQuery(query);
		srq.addMatchingModel("Matching" , "BE_LM");
		queryingManager.runSearchRequest(srq); 
		ResultSet results = srq.getResultSet();

		System.out.println(results.getExactResultSize()+" documents were scored");
		System.out.println("The top "+results.getResultSize()+" of those documents were returned");

		
		try {
			//check the existence of the directory
			File f = new File(resultDirectory);
			if(!f.exists()) {
				f.mkdirs();
			}
			
			// Print the results
			Path outputPath = Paths.get(resultDirectory + "/blanco.res");
			BufferedWriter writer = Files.newBufferedWriter(outputPath, UsefulConstants.CHARSET_ENCODING);
			
			for (int i =0; i< results.getResultSize(); i++) {
				int docid = results.getDocids()[i];
				//identifier of the document
				String docno = index.getMetaIndex().getItem("docno", docid);
				double score = results.getScores()[i];
				//write on Standard output:
//				System.out.println("   Rank "+i+" docid: "+ docno + " "+" "+score);
				//write on file:
				writer.write("query_no Q0 " + docno + " " + i + " " + score + " BE_LM.beta=0.9");
				writer.newLine();
			}
			
			//close the writer 
			writer.close();
			//XXX
			//clean the memory of the open pointers to the indexes used in the BE_LM
			//this is really important for a massive usage of the class
			//should I write it down somewhere? Probably yes.
			BE_LM_StaticVariablesHolder.reset();
			
		} catch(NoSuchFileException e) {
			//print at least on the standard output (cribbio, mi consenta)
			
			for (int i =0; i< results.getResultSize(); i++) {
				int docid = results.getDocids()[i];
				//identifier of the document
				String docno = index.getMetaIndex().getItem("docno", docid);
				double score = results.getScores()[i];
				//write on Standard output:
				System.out.println("   Rank "+i+" docid: "+ docno + " "+" "+score);
				//write on file:
			}
		}


	} 

	//
	
	/** Testing main
	 * */
	public static void main(String[] args) throws IOException {
		AnswerSubgraphsRankingPhase phase = new AnswerSubgraphsRankingPhase();
		phase.subgraphsRankingPhase();
	}

	public String getResultDirectory() {
		return resultDirectory;
	}

	public void setResultDirectory(String resultDirectory) {
		this.resultDirectory = resultDirectory;
	}

	public String getIndexPath() {
		return indexPath;
	}

	public void setIndexPath(String indexPath) {
		this.indexPath = indexPath;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}
}
