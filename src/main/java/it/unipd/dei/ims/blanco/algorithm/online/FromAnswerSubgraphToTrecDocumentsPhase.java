package it.unipd.dei.ims.blanco.algorithm.online;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;

import org.apache.commons.io.FileUtils;
import org.openrdf.model.Literal;
import org.openrdf.model.Value;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.RDFParseException;
import org.openrdf.rio.RDFParser;
import org.openrdf.rio.Rio;
import org.openrdf.rio.helpers.StatementCollector;

import it.unipd.dei.ims.datastructure.ThreadState;
import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;
import it.unipd.dei.ims.rum.utilities.StringUsefulMethods;
import it.unipd.dei.ims.rum.utilities.UrlUtilities;
import it.unipd.dei.ims.rum.utilities.UsefulConstants;

/** Phase 6 of the Blanco algorithm. 
 * <p>
 * Translation of the subgraphs in
 * TREC documents.*/
public class FromAnswerSubgraphToTrecDocumentsPhase {

	/**integer that keeps track of the number of TREC files we have produced.
	 * */
	private int fileCounter;
	
	/** Path of the directory where we find the sub-directories with the RDF
	 * files produced in phase 5.
	 * */
	private String answerSubgraphsMainDirectory;
	
	/** Path where to save the output TREC files.
	 * */
	private String outputDirectory;
	
	public FromAnswerSubgraphToTrecDocumentsPhase () {
		this.fileCounter = 0;
		
		try {
			this.setup();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void setup() throws IOException {
		Map<String, String> map = 
				PropertiesUsefulMethods.getSinglePropertyFileMap("properties/main.properties");
		this.answerSubgraphsMainDirectory = map.get("answer.subgraphs.main.directory");
		this.outputDirectory = map.get("output.directory");
	}
	
	/** Converts the files RDF in turtle format inside a directory (and subdirectories)
	 * into files.
	 * 
	 * */
	public void convertRDFAnswerSubgraphsInTRECDocuments() {
		File outDire = new File(outputDirectory);
		if(!outDire.exists())
			outDire.mkdirs();

		//clean the directory
		try {
			FileUtils.cleanDirectory(new File(outputDirectory));
		} catch (IOException e1) {
			e1.printStackTrace();
			System.err.println("unable to clean the directory");
		}

		//a map to keep track of the paths
		Map<String, String> pathMap = new HashMap<String, String>();
		
		//open the main directory
		File mainDir = new File(this.answerSubgraphsMainDirectory);
		
		
		if(! mainDir.isDirectory()) {
			throw new IllegalArgumentException("provied path is not a directory");
		}
		//queue to keep track of the directories we still have to visit
		Queue<File> directoryQueue = new LinkedList<File>();
		directoryQueue.add(mainDir);
		while(!directoryQueue.isEmpty()) {
			File f = directoryQueue.remove();
			File[] files = f.listFiles();
			for (File file : files) {
				if(file.isDirectory()) {
					//add the directory to the ones we have to visit
					directoryQueue.add(file);
				}
				else {
					//it is a rightful file, add to the map
					String path = file.getAbsolutePath();
					String name = file.getName();
					if(name.equals(".DS_Store"))
						continue;
					String docId = "";
					try {
						docId = StringUsefulMethods.getIdFromFile(file);
					} catch(Exception e) {
						System.err.println("Error in the regular expression");
					}
					pathMap.put(docId, path);
				}
			}
			if(pathMap.size() >= 2048) {
				try {
					if(Thread.interrupted()) {
						ThreadState.setOnLine(false);
						return;
					}
					
					fileCounter++;
					convertAllFilesInTheMap(pathMap, outputDirectory, fileCounter);
					//clean the map
					pathMap.clear();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (RDFParseException e) {
					e.printStackTrace();
				} catch (RDFHandlerException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}//end if
		}//end external while
		
		if(pathMap.size() > 0) {
			try {
				if(Thread.interrupted()) {
					ThreadState.setOnLine(false);
					return;
				}
				
				fileCounter++;
				convertAllFilesInTheMap(pathMap, outputDirectory, fileCounter);
				//clean the map
				pathMap.clear();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (RDFParseException e) {
				e.printStackTrace();
			} catch (RDFHandlerException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}//end if
		
	}
	
	/** Given a map containing files, the method converts all the files putting them in a unique trec file. 
	 * 
	 * @param fileCounter name to give to the TREC file
	 * 
	 * @throws IOException 
	 * @throws RDFHandlerException 
	 * @throws RDFParseException 
	 * 
	 * */
	private static void convertAllFilesInTheMap(Map<String, String> pathMap, String outputDirectory, int fileCounter) 
			throws RDFParseException, RDFHandlerException, IOException {
		
		
		//name of the output file
		String outputFile = outputDirectory + "/" + fileCounter + ".trec";
		System.out.println("printing the file " + outputFile);
		Path outputPath = Paths.get(outputFile);
		BufferedWriter writer = Files.newBufferedWriter(outputPath, UsefulConstants.CHARSET_ENCODING);
		
		for(Entry<String, String> entry : pathMap.entrySet()) {
			String path = entry.getValue();
			String id = entry.getKey();
			
			//now read the file
			//open the input stream to the file
			InputStream inputStream = new FileInputStream(new File(path));
			//prepare a collector to contain the triples
			StatementCollector collector = new StatementCollector();
			//read the file
			RDFParser rdfParser = Rio.createParser(RDFFormat.TURTLE);
			//link the collector to the parser
			rdfParser.setRDFHandler(collector);
			//parse the file
			rdfParser.parse(inputStream, "");
			//now get the triples/statements composing the graph
			Collection<org.openrdf.model.Statement> statements = collector.getStatements();
			//print the graph in TREC format
			printAFile(statements, id, writer);
			
			inputStream.close();
		}
		
		writer.close();
		
		

	}
	
	private static void printAFile(Collection<org.openrdf.model.Statement> statements, String id, BufferedWriter writer) throws IOException {
		//write the file
		writer.write("<DOC>");
		writer.newLine();

		writer.write("<DOCNO>" + id + "</DOCNO>");
		writer.newLine();
		writeADocument(writer, statements);
		writer.newLine();

		writer.write("</DOC>");
		writer.newLine();
		writer.flush();

	}
	
	
	/** Given a graph, it uses the provided writer to convert this graph in a TREC
	 * document and print it in memory.
	 * */
	private static void writeADocument(BufferedWriter writer, Collection<org.openrdf.model.Statement> graph) throws IOException {
		for(org.openrdf.model.Statement t : graph) {
			//for each triple, we print subject, predicate and object
			Value subject = t.getSubject();
			Value predicate = t.getPredicate();
			Value object = t.getObject();
			
			//subject
			String subjectString = UrlUtilities.takeWordsFromIri(subject.toString());
			subjectString = StringUsefulMethods.checkCharacterInStringForTREC(subjectString);
			writer.write( subjectString + " ");
			
			//predicate
			String predicateString = UrlUtilities.takeWordsFromIri(predicate.toString());
			predicateString = StringUsefulMethods.checkCharacterInStringForTREC(predicateString);
			writer.write(predicateString + " ");
			
			//object
			if(object instanceof Literal) {
				String objectString = StringUsefulMethods.checkCharacterInStringForTREC(object.stringValue());
				writer.write(objectString + " ");
			}
			else {
				String objectString = UrlUtilities.takeWordsFromIri(object.toString());
				objectString = StringUsefulMethods.checkCharacterInStringForTREC(objectString);
				writer.write(objectString.trim() + " ");
			}

		}
	}
	
	/** test main
	 * */
	public static void main(String[] args) {
		FromAnswerSubgraphToTrecDocumentsPhase phase = new FromAnswerSubgraphToTrecDocumentsPhase();
		phase.convertRDFAnswerSubgraphsInTRECDocuments();
	}

	public String getAnswerSubgraphsMainDirectory() {
		return answerSubgraphsMainDirectory;
	}

	public void setAnswerSubgraphsMainDirectory(String answerSubgraphsMainDirectory) {
		this.answerSubgraphsMainDirectory = answerSubgraphsMainDirectory;
	}

	public String getOutputDirectory() {
		return outputDirectory;
	}

	public void setOutputDirectory(String outputDirectory) {
		this.outputDirectory = outputDirectory;
	}
	
}
