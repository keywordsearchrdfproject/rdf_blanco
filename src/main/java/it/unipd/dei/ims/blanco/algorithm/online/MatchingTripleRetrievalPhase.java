package it.unipd.dei.ims.blanco.algorithm.online;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.Map.Entry;

import org.terrier.querying.Manager;
import org.terrier.querying.SearchRequest;
import org.terrier.structures.Index;
import org.terrier.structures.IndexOnDisk;

import it.unipd.dei.ims.datastructure.ConnectionHandler;
import it.unipd.dei.ims.datastructure.DatabaseState;
import it.unipd.dei.ims.datastructure.ThreadState;
import it.unipd.dei.ims.rum.utilities.UsefulConstants;
import it.unipd.dei.ims.terrier.utilities.PropertiesUsefulMethods;

/**Blanco algorithm: phase 3
 * 
 * <p> 
 * 
 * This class utilizes the Terrier Index to retrieve all the triples in the graph
 * that contains at least one keyword.
 * <p>
 * property file: properties/matchingTripleTetrievalPhase.properties
 * and properties.query_list.properties
 * <p>
 * In the query_list.properties file you can insert all your queries, in order to execute this method only once
 * for all the query you have.
 * 
 * */
public class MatchingTripleRetrievalPhase {

	/** Path of the terrier index to be used to find the triples*/
	private String indexPath;

	private String jdbcConnectingString;

	/**where to save the res file with the triples
	 * <p>
	 * property: output.directory*/
	private String outputDirectory;

	/** Model we want to use to check the elements
	 * <p>
	 * property: model
	 * */
	private String model;

	/** Name of the database we are working on
	 * <p>
	 * property: database.name
	 * */
	private String databaseName;

	/** keyword Query*/
	private String query;
	
	private String queryId;
	
	private int maxAllowedTriples;

	private static final String SQL_GET_NUMBER_OF_TRIPLES = "SELECT triple_number FROM "
			+ DatabaseState.getSchema() + ".metadata where database_name = ?";


	public MatchingTripleRetrievalPhase() {
		try {
			queryId = "0";
			this.setup();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void setup() throws IOException {

		Map<String, String> map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/main.properties");
		indexPath = map.get("trec.index.path");
		String terrierHome = map.get("terrier.home");
		String terrierEtc = map.get("terrier.etc");

		//setting system properties for terrier to find the terrier_home and the /etc directory
		System.setProperty("terrier.home", terrierHome);
		System.setProperty("terrier.etc", terrierEtc);

		jdbcConnectingString = map.get("jdbc.connection.string");
		outputDirectory = map.get("output.queries.directory");
		model = map.get("model");

		databaseName = map.get("database.name");

		this.query = map.get("query");
		
		this.maxAllowedTriples = Integer.parseInt(map.get("max.allowed.triples"));
	}

	/** This method reads from a property file all the queries
	 * that we want to do and finds out the triples that matches at least one
	 * query word. 
	 * */
	public void matchMultipleTriples() {

		//open the index and get the query manager
		Index index = IndexOnDisk.createIndex(indexPath, "data");
		Manager queryingManager = new Manager(index);

		Connection rdbConnection = null;

		try {
			//XXX SQL
//			rdbConnection = DriverManager.getConnection(jdbcConnectingString);
			rdbConnection = ConnectionHandler.createConnectionAsOwner(jdbcConnectingString, this.getClass().getName());
			
			PreparedStatement stmt = rdbConnection.prepareStatement(SQL_GET_NUMBER_OF_TRIPLES);
			stmt.setString(1, this.databaseName);
			ResultSet rs = stmt.executeQuery();
			int numberOfTriples = 0;
			if(rs.next()) {
				numberOfTriples = rs.getInt("triple_number");
				//need to set these properties in order to take all the matching triples
				System.setProperty("trec.output.format.length", "" + numberOfTriples);
				System.setProperty("matching.retrieved_set_size", "" + numberOfTriples);
			}

			//now take the queries to check
			Map<String, String> map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/query_list.properties");

			for(Entry<String, String> entry : map.entrySet()) {
				String id = entry.getKey();
				String query = entry.getValue();

				System.out.println("now doing query: " + query);

				SearchRequest srq = queryingManager.newSearchRequestFromQuery(query);
				srq.addMatchingModel("Matching" , model);

				queryingManager.runSearchRequest(srq);
				org.terrier.matching.ResultSet results = srq.getResultSet();

				//path where to save the results of the query
				//				Path outputPath = Paths.get(resultDirectory + "/" + id + ".res");
				String filePath = this.outputDirectory + "/" + id ;
				File queryDir = new File(filePath);
				if(!queryDir.exists()) {
					queryDir.mkdirs();
				}
				//path of the file where to write the results
				Path outputPath = Paths.get(filePath + "/matching_triples.txt");

				try (BufferedWriter writer = Files.newBufferedWriter(outputPath, UsefulConstants.CHARSET_ENCODING)){
					System.out.println("Document Ranking");
					for (int i =0; i< results.getResultSize(); i++) {
						int docid = results.getDocids()[i];
						//identifier of the document
						String docno = index.getMetaIndex().getItem("docno", docid);
						double score = results.getScores()[i];
						writer.write("query_no " + id + " " + docno + " " + i + " " + score + " " + model);
						writer.newLine();
					}
					writer.close();
				} catch (IOException e) {
					e.printStackTrace();
				}

			}
			ConnectionHandler.closeConnectionIfOwner(this.getClass().getName());
			//XXX
//			rdbConnection.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}


	}
	
	/** This algorithm uses Tf to find all the triples in the
	 * graph that matches at least on query word, then it uses 
	 * TF_IDF to select the top promising triples, taking
	 * one third of the whole collection of mathcing triples.
	 * This is done to improve performances.
	 * */
	public void findMatchingTriplesWithPruning () {
		//open the index and get the query manager
		Index index = IndexOnDisk.createIndex(indexPath, "data");
		Manager queryingManager = new Manager(index);

		Connection rdbConnection = null;

		try {
			//XXX SQL
			rdbConnection = ConnectionHandler.createConnectionAsOwner(jdbcConnectingString, this.getClass().getName());
			
			PreparedStatement stmt = rdbConnection.prepareStatement(SQL_GET_NUMBER_OF_TRIPLES);
			stmt.setString(1, DatabaseState.getSchema());
			ResultSet rs = stmt.executeQuery();
			int numberOfTriples = 0;
			if(rs.next()) {
				numberOfTriples = rs.getInt("triple_number");
				//need to set these properties in order to take all the matching triples
				System.setProperty("trec.output.format.length", "" + numberOfTriples);
				System.setProperty("matching.retrieved_set_size", "" + numberOfTriples);
			}

			SearchRequest srq = queryingManager.newSearchRequestFromQuery(this.query);
			srq.addMatchingModel("Matching" , model);

			queryingManager.runSearchRequest(srq);
			org.terrier.matching.ResultSet results = srq.getResultSet();
			
			//get the number of matching triples
			int resultSize = results.getResultSize();
			
			//we don't want the whole set of matching triples because they are too many.
			//we reduce the number taking one third (heuristic)
			resultSize = Math.min(resultSize / 3, this.maxAllowedTriples);
			
			//execute the query again, with a different model
			srq = queryingManager.newSearchRequestFromQuery(this.query);
			srq.addMatchingModel("Matching" , "TF_IDF");
//			srq.addMatchingModel("Matching" , "BM25");

			queryingManager.runSearchRequest(srq);
			results = srq.getResultSet();

			//path where to save the results of the query
			//						Path outputPath = Paths.get(resultDirectory + "/" + id + ".res");
			String filePath = this.outputDirectory  ;
			File queryDir = new File(filePath);
			if(!queryDir.exists()) {
				queryDir.mkdirs();
			}
			//path of the file where to write the results
			Path outputPath = Paths.get(filePath + "/matching_triples.txt");

			try (BufferedWriter writer = Files.newBufferedWriter(outputPath, UsefulConstants.CHARSET_ENCODING)){
				System.out.println("Document Ranking");
				//print the top results of the query
				for (int i =0; i< resultSize; i++) {
					int docid = results.getDocids()[i];
					//identifier of the document
					String docno = index.getMetaIndex().getItem("docno", docid);
					double score = results.getScores()[i];
					writer.write("query_no " + 1 + " " + docno + " " + i + " " + score + " " + model);
					writer.newLine();
				}
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			ConnectionHandler.closeConnectionIfOwner(this.getClass().getName());

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/** Find the matching triples for a single query.
	 * THe method finds all the triples in the graph
	 * that contain at least one keyword 
	 * using the Tf model from Terrier.
	 * 
	 * */
	public void findMatchingTriples () {
		//open the index and get the query manager
		Index index = IndexOnDisk.createIndex(indexPath, "data");
		Manager queryingManager = new Manager(index);

		Connection rdbConnection = null;

		try {
			rdbConnection  = ConnectionHandler.createConnectionAsOwner(
					jdbcConnectingString, 
					this.getClass().getName());
			PreparedStatement stmt = rdbConnection.prepareStatement(SQL_GET_NUMBER_OF_TRIPLES);
			stmt.setString(1, this.databaseName);
			ResultSet rs = stmt.executeQuery();
			int numberOfTriples = 0;
			if(rs.next()) {
				numberOfTriples = rs.getInt("triple_number");
				//need to set these properties in order to take all the matching triples
				System.setProperty("trec.output.format.length", "" + numberOfTriples);
				System.setProperty("matching.retrieved_set_size", "" + numberOfTriples);
			}

			SearchRequest srq = queryingManager.newSearchRequestFromQuery(this.query);
			srq.addMatchingModel("Matching" , model);//model should be "Tf" from the properties

			queryingManager.runSearchRequest(srq);
			org.terrier.matching.ResultSet results = srq.getResultSet();

			//path where to save the results of the query
			String filePath = this.outputDirectory  ;
			File queryDir = new File(filePath);
			if(!queryDir.exists()) {
				queryDir.mkdirs();
			}
			//path of the file where to write the results
			Path outputPath = Paths.get(filePath + "/matching_triples.txt");

			try (BufferedWriter writer = Files.newBufferedWriter(outputPath, UsefulConstants.CHARSET_ENCODING)){
				System.out.println("Document Ranking");
				for (int i =0; i< results.getResultSize(); i++) {
					
					if(Thread.interrupted()) {
						ThreadState.setOnLine(false);
						writer.close();
						return;
					}
					
					int docid = results.getDocids()[i];
					//identifier of the document
					String docno = index.getMetaIndex().getItem("docno", docid);
					double score = results.getScores()[i];
					writer.write("query_no " + 1 + " " + docno + " " + i + " " + score + " " + model);
					writer.newLine();
				}
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			ConnectionHandler.closeConnectionIfOwner(this.getClass().getName());
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void test(String[] args) {
		MatchingTripleRetrievalPhase phase = new MatchingTripleRetrievalPhase();
//		phase.findMatchingTriples();
		phase.findMatchingTriplesWithPruning();
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public String getOutputDirectory() {
		return outputDirectory;
	}

	public void setOutputDirectory(String outputDirectory) {
		this.outputDirectory = outputDirectory;
	}

	public String getQueryId() {
		return queryId;
	}

	public void setQueryId(String queryId) {
		this.queryId = queryId;
	}

	public String getIndexPath() {
		return indexPath;
	}

	public void setIndexPath(String indexPath) {
		this.indexPath = indexPath;
	}


}
