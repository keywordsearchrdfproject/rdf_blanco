package it.unipd.dei.ims.blanco.algorithm.online;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import it.unipd.dei.ims.datastructure.ConnectionHandler;
import it.unipd.dei.ims.datastructure.DatabaseState;
import it.unipd.dei.ims.datastructure.ThreadState;
import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;
import it.unipd.dei.ims.rum.utilities.SQLUtilities;

/** Blanco algorithm: phase 4
 * <p>
 * This class represents the phase in which we take
 * the triples that contain
 * <p>
 * NB: in the Query Graph table the id is not a serial, we create it in order to be synchronized 
 * with the ids in the trec collection of triples.
 * */
public class QueryGraphGenerationPhase {

	private static final String SQL_GET_TRIPLE = "SELECT id_, subject_, predicate_, object_" + 
			"	FROM " + DatabaseState.getSchema() + ".triple_store where id_=?;";

	private static final String SQL_TRUNCATE_QUERY_GRAPH_TABLE = "truncate table "
			 + DatabaseState.getSchema() + ".query_graph";

	private static final String SQL_INSERT_IN_QUERY_GRAPH = "INSERT INTO "
			+ DatabaseState.getSchema() + ".query_graph(" + 
			"	id_, subject_, predicate_, object_)" + 
			"	VALUES (?, ?, ?, ?);";

	/** The path of the file .res containing the list of id of triples
	 * obtained from the {@link MatchingTripleRetrievalPhase}.
	 * <p>
	 * property: result.source.file.path*/
	private String resultSourceFilePath;

	private String jdbcConnectionString;

	public QueryGraphGenerationPhase () {
		this.setup();
	}


	public void setup() {
		try {
			Map<String, String> map = 
					PropertiesUsefulMethods.getSinglePropertyFileMap("properties/main.properties");
			resultSourceFilePath = map.get("result.source.file.path");
			this.jdbcConnectionString = map.get("jdbc.connection.string");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/** Re-create the graph inside a table in a RDB database*/
	public void createQueryGraphInDatabase() {
		Connection connection;

		//clean the actual query_graph table from the previous query
		java.sql.Statement st;
		try {
			//XXX SQL
//			connection = SQLUtilities.getRDBConnection(jdbcConnectionString);
			connection = ConnectionHandler.createConnectionAsOwner(jdbcConnectionString, this.getClass().getName());
			st = connection.createStatement();
			System.out.println("deleting all data...");
			int result = st.executeUpdate(SQL_TRUNCATE_QUERY_GRAPH_TABLE);
			System.out.println("data deleted");

			//get the file .res with all the informations
			Path inputPath = Paths.get(this.resultSourceFilePath);

			try(BufferedReader reader = Files.newBufferedReader(inputPath)) {
				//read the .res file
				String line = "";
				int stmtCounter = 0, totalCounter = 0;

				PreparedStatement insertStatement = connection.prepareStatement(SQL_INSERT_IN_QUERY_GRAPH);
				while( (line=reader.readLine()) != null) {
					//the lines are in the form
					//query_no Q0 ID rank score model
					String[] parts = line.split(" ");
					String id_ = parts[2];

					//take the information of the triple
					Map<String, String> tripleMap = getTripleInformationsFromIdAsMap(connection, id_);

					//insert the triple in the table E.
					insertStatement.setInt(1, Integer.parseInt(tripleMap.get("id_")));
					insertStatement.setString(2, tripleMap.get("subject_"));
					insertStatement.setString(3, tripleMap.get("predicate_"));
					insertStatement.setString(4, tripleMap.get("object_"));

					insertStatement.addBatch();

					stmtCounter++;
					if(stmtCounter>=50000) {
						
						if(Thread.interrupted()) {
							ThreadState.setOnLine(false);
							return;
						}
						
						totalCounter+=stmtCounter;
						stmtCounter=0;
						System.out.println("inserting triples to the graph E. Currently we are at: " + totalCounter);
						insertStatement.executeBatch();
						insertStatement.clearBatch();
					}
				}
				if(stmtCounter>0) {
					
					if(Thread.interrupted()) {
						ThreadState.setOnLine(false);
						return;
					}
					
					totalCounter+=stmtCounter;
					stmtCounter=0;
					System.out.println("inserting last triples to the graph E. We are at: " + totalCounter);
					insertStatement.executeBatch();
					insertStatement.clearBatch();
				}
				ConnectionHandler.closeConnectionIfOwner(this.getClass().getName());
			} catch (IOException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {

			}
		} catch (SQLException e1) {
			e1.printStackTrace();
		} finally {

		}

	}

	/** given the connection to our RDB and the id of a triple in the triple_store table, 
	 * returns a map with the informations about the id, the subject, the predicate and the object of the triple.
	 * <p>
	 * The keys used are id_, subject_, predicate_, object_
	 * 
	 * @throws SQLException 
	 * 
	 * */
	private static Map<String, String> getTripleInformationsFromIdAsMap(Connection connection, String id_) throws SQLException {
		//get the triple from the RDB
		PreparedStatement selectTripleStmt;
		selectTripleStmt = connection.prepareStatement(SQL_GET_TRIPLE);
		selectTripleStmt.setInt(1, Integer.parseInt(id_));
		ResultSet rs = selectTripleStmt.executeQuery();
		rs.next();

		String sbj = rs.getString("subject_");
		String pr = rs.getString("predicate_");
		String obj = rs.getString("object_");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id_", id_);
		map.put("subject_", sbj);
		map.put("predicate_", pr);
		map.put("object_", obj);
		
		return map;

	}
	
	
	
	/////
	
	/** Main method to test the class*/
	public static void main(String[] args) {

		QueryGraphGenerationPhase phase = new QueryGraphGenerationPhase();
		phase.createQueryGraphInDatabase();
	}


	public String getResultSourceFilePath() {
		return resultSourceFilePath;
	}


	public void setResultSourceFilePath(String resultSourceFilePath) {
		this.resultSourceFilePath = resultSourceFilePath;
	}


}
