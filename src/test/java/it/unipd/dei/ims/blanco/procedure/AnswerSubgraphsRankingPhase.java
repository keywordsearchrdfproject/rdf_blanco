package it.unipd.dei.ims.blanco.procedure;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

import org.terrier.matching.ResultSet;
import org.terrier.querying.Manager;
import org.terrier.querying.SearchRequest;
import org.terrier.structures.Index;
import org.terrier.structures.IndexOnDisk;

import it.unipd.dei.ims.rum.utilities.UsefulConstants;
import it.unipd.dei.ims.terrier.utilities.PropertiesUsefulMethods;

/** Phase 7 of the Blanco algorithm.
 * Once created the index of the subgraphs (you can do that via batch command
 * with Terrier or with {@link TrecIndexPhase}), we need to rank them in order
 * to return them to the user.
 * <p>
 * This class uses a custom made Language Model in order to generate this ranking.
 * */
public class AnswerSubgraphsRankingPhase {

	
	public static void subgraphsRankingPhase() throws IOException {
		//take the useful information by the properties file
		Map<String, String> map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/trec_ranking_phase.properties");
		
		//get the terrier home and etc directory where we have the property file and set them
		String terrierHome = map.get("terrier.home");
		String terrierEtc = map.get("terrier.etc");
		
		System.setProperty("terrier.home", terrierHome);
		System.setProperty("terrier.etc", terrierEtc);
		
		//set the number of results we want 
		System.setProperty("trec.output.format.length", ""+1000);
		System.setProperty("matching.retrieved_set_size", ""+1000);
		
		String resultDirectory = map.get("result.directory");
		String indexPath = map.get("trec.index.path");
		
		//open the index
		Index index = IndexOnDisk.createIndex(indexPath, "data");
		System.out.println("We have indexed " + index.getCollectionStatistics().getNumberOfDocuments() + " documents");
		
		String query = map.get("query");
		Manager queryingManager = new Manager(index);
		
		
		
		
		
		//execute the query
		SearchRequest srq = queryingManager.newSearchRequestFromQuery(query);
		srq.addMatchingModel("Matching" , "BE_LM");
//		srq.addMatchingModel("Matching" , "BM25");
		queryingManager.runSearchRequest(srq);
		ResultSet results = srq.getResultSet();
		
		System.out.println(results.getExactResultSize()+" documents were scored");
		System.out.println("The top "+results.getResultSize()+" of those documents were returned");
		
		// Print the results
		Path outputPath = Paths.get(resultDirectory + "/blanco.res");
		BufferedWriter writer = Files.newBufferedWriter(outputPath, UsefulConstants.CHARSET_ENCODING);
		
		for (int i =0; i< results.getResultSize(); i++) {
			int docid = results.getDocids()[i];
			//identifier of the document
			String docno = index.getMetaIndex().getItem("docno", docid);
			double score = results.getScores()[i];
			//write on Standard output:
			System.out.println("   Rank "+i+" docid: "+ docno + " "+" "+score);
			//write on file:
			writer.write("query_no Q0 " + docno + " " + i + " " + score + " BE_LM.beta=0.9");
			writer.newLine();
		}
		
		writer.close();
		
		
	} 
	
	public static void main(String[] args) throws IOException {
		AnswerSubgraphsRankingPhase.subgraphsRankingPhase();
	}
}
