package it.unipd.dei.ims.blanco.procedure;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.Value;
import org.openrdf.model.impl.StatementImpl;
import org.openrdf.model.impl.TreeModel;
import org.openrdf.model.impl.URIImpl;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;

import it.unipd.dei.ims.terrier.utilities.BlazegraphUsefulMethods;
import it.unipd.dei.ims.terrier.utilities.PropertiesUsefulMethods;
import it.unipd.dei.ims.terrier.utilities.SQLUtilities;
import it.unipd.dei.ims.terrier.utilities.UrlUtilities;

/** Phase 4 of the Blanco algorithm. This class takes a text file (.res)
 * containing the triples of the graph that contain at least 1 keyword.
 * It creates a TDB containing all these triples. 
 *  */
public class QueryGraphGeneratorE {

	private static final String SQL_GET_TRIPLE = "SELECT id_, subject_, predicate_, object_" + 
			"	FROM public.triple_store where id_=?;";
	
	private static final String SQL_TRUNCATE_QUERY_GRAPH_TABLE = "truncate table query_graph";
	
	private static final String SQL_INSERT_IN_QUERY_GRAPH = "INSERT INTO public.query_graph(" + 
			"	id_, subject_, predicate_, object_)" + 
			"	VALUES (?, ?, ?, ?);";


	/** Creates a list of ordered ID representing the elements of the Query Graph E.
	 *
	 * @param queryDirectoryPath string with the path of the directory with all the files of the query
	 * */
	public static List<Integer> getListOfTripleIDsOfQueryGraph(String queryDirectoryPath) {
		List<Integer> idList = new ArrayList<Integer>();

		Path inputPath = Paths.get(queryDirectoryPath + "/matching_triples.res");
		try(BufferedReader reader = Files.newBufferedReader(inputPath)) {
			String line = "";
			int lineCounter = 0;
			//read one line and get the ID
			while( (line=reader.readLine()) != null) {
				String[] parts = line.split(" ");
				String id_ = parts[2];
				//insert the id in the list
				idList.add(Integer.parseInt(id_));
				lineCounter++;
				if(lineCounter%10000==0)
					System.out.println("Read " + lineCounter + " id of the query graph");
			}

			System.out.println("ordering the ids...");
			Collections.sort(idList);
			System.out.print("ordering completed.");

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return idList;

	}


	/** Given the path to a directory containing the files of a query, this method uses the support relational database to crete
	 * the query graph E and save it into memory as triple store.
	 * 
	 * @param queryDirectoryPath string with the path of the directory with all the files of the query
	 * @param connection a Connection object to the support relational database.
	 * */
	public static void createOneQueryGraphTDB(String queryDirectoryPath, Connection connection) {

		//open/create the blazegraph repository representing the graph E
		org.openrdf.repository.Repository repository = BlazegraphUsefulMethods.createRepository(queryDirectoryPath + "/E.jnl");
		org.openrdf.model.Model model = new TreeModel();

		RepositoryConnection rc = null;
		try {
			repository.initialize();
			//			rc = BlazegraphUtilities.getRepositoryConnection(repository);
			rc = repository.getConnection();

			//get the file .res with all the informations
			Path inputPath = Paths.get(queryDirectoryPath + "/matching_triples.res");
			try(BufferedReader reader = Files.newBufferedReader(inputPath)) {
				//read the .res file
				String line = "";
				//				rc.begin();
				int stmtCounter = 0, totalCounter = 0;
				while( (line=reader.readLine()) != null) {
					//the lines are in the form
					//query_no Q0 ID rank score model
					String[] parts = line.split(" ");
					String id_ = parts[2];

					Statement t = getTripleFromId(connection, id_);
					rc.add(t);
					stmtCounter++;
					if(stmtCounter>=50000) {
						totalCounter+=stmtCounter;
						System.out.println("added " + totalCounter + " triples to the graph E");
						stmtCounter=0;
						rc.commit();
					}
				}
				if(stmtCounter>0) {
					stmtCounter=0;
					rc.commit();
				}
			} catch (IOException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {

			}
			rc.close();
			repository.shutDown();
		} catch (RepositoryException e1) {
			try {
				rc.rollback();
			} catch (RepositoryException e) {
				e.printStackTrace();
			}
			e1.printStackTrace();
		} finally {

		}

	}

	/** Given the path to a directory containing the files of a query, this method uses the support relational database to crete
	 * the query graph E and save it in another table
	 * 
	 * @param queryDirectoryPath string with the path of the directory with all the files of the query
	 * @param connection a Connection object to the support relational database.
	 * */
	public static void createOneQueryGraphInDatabase(String queryDirectoryPath, Connection connection) {
		try {
			//clean the actual query_graph table from the previous query
			java.sql.Statement st = connection.createStatement();
			System.out.print("deleting all data...");
			int result = st.executeUpdate(SQL_TRUNCATE_QUERY_GRAPH_TABLE);
			System.out.println("data deleted");
			
			//get the file .res with all the informations
			Path inputPath = Paths.get(queryDirectoryPath + "/blanco_support/matching_triples.res");
			try(BufferedReader reader = Files.newBufferedReader(inputPath)) {
				//read the .res file
				String line = "";
				int stmtCounter = 0, totalCounter = 0;
				
				PreparedStatement insertStatement = connection.prepareStatement(SQL_INSERT_IN_QUERY_GRAPH);
				while( (line=reader.readLine()) != null) {
					//the lines are in the form
					//query_no Q0 ID rank score model
					String[] parts = line.split(" ");
					String id_ = parts[2];

					//take the information of the triple
					Map<String, String> tripleMap = getTripleInformationsFromIdAsMap(connection, id_);
					
					//insert the triple in the table E.
					insertStatement.setInt(1, Integer.parseInt(tripleMap.get("id_")));
					insertStatement.setString(2, tripleMap.get("subject_"));
					insertStatement.setString(3, tripleMap.get("predicate_"));
					insertStatement.setString(4, tripleMap.get("object_"));
					
					insertStatement.addBatch();
					
					stmtCounter++;
					if(stmtCounter>=50000) {
						totalCounter+=stmtCounter;
						stmtCounter=0;
						System.out.println("inserting triples to the graph E. Currently we are at: " + totalCounter);
						insertStatement.executeBatch();
						insertStatement.clearBatch();
					}
				}
				if(stmtCounter>0) {
					totalCounter+=stmtCounter;
					stmtCounter=0;
					System.out.println("inserting last triples to the graph E. We are at: " + totalCounter);
					insertStatement.executeBatch();
					insertStatement.clearBatch();
				}
			} catch (IOException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {

			}
		} catch (SQLException e1) {
			e1.printStackTrace();
		} finally {

		}

	}

	private static Statement getTripleFromId(Connection connection, String id_) throws SQLException {
		PreparedStatement selectTripleStmt;
		selectTripleStmt = connection.prepareStatement(SQL_GET_TRIPLE);
		selectTripleStmt.setInt(1, Integer.parseInt(id_));
		ResultSet rs = selectTripleStmt.executeQuery();
		rs.next();

		String sbj = rs.getString("subject_");
		String pr = rs.getString("predicate_");
		String obj = rs.getString("object_");

		//create the triple statement
		URI subject = new URIImpl(sbj);
		URI predicate = new URIImpl(pr);
		Value object;
		if(UrlUtilities.checkIfValidURL(obj)) {
			//obj is a URL
			object = new URIImpl(obj);
		} else {
			object = BlazegraphUsefulMethods.dealWithTheObjectLiteralString(obj);
		}

		Statement stat = new StatementImpl(subject, predicate, object);
		return stat;
	}

	/** given the connection to our RDB and the id of a triple in the triple_store table, 
	 * returns a map with the informations about the id, the subject, the predicate and the object of the triple.
	 * <p>
	 * The keys used are id_, subject_, predicate_, object_
	 * 
	 * @throws SQLException 
	 * 
	 * */
	private static Map<String, String> getTripleInformationsFromIdAsMap(Connection connection, String id_) throws SQLException {
		//get the triple from the RDB
		PreparedStatement selectTripleStmt;
		selectTripleStmt = connection.prepareStatement(SQL_GET_TRIPLE);
		selectTripleStmt.setInt(1, Integer.parseInt(id_));
		ResultSet rs = selectTripleStmt.executeQuery();
		rs.next();

		String sbj = rs.getString("subject_");
		String pr = rs.getString("predicate_");
		String obj = rs.getString("object_");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id_", id_);
		map.put("subject_", sbj);
		map.put("predicate_", pr);
		map.put("object_", obj);
		
		return map;

	}

	//#######################

	public static void main(String[] args) throws IOException {
		Map<String, String> map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/convertion.properties");
		String jdbcConnectionString = map.get("jdbc.connection.string");

		//get the list of all the query directories
		map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/query_directories.properties");

		Connection connection = SQLUtilities.getRDBConnection(jdbcConnectionString);
		for(Entry<String, String> entry : map.entrySet()) {
			//actually, there will be only 1 directory per time. THe grah E is necessary also in the phase 5 
			String queryDirectory = entry.getValue();
//			QueryGraphGeneratorE.createOneQueryGraphTDB(queryDirectory, connection);
			QueryGraphGeneratorE.createOneQueryGraphInDatabase(queryDirectory, connection);
		}

	}
}
