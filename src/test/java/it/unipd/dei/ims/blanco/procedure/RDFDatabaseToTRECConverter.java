package it.unipd.dei.ims.blanco.procedure;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import it.unipd.dei.ims.terrier.utilities.PropertiesUsefulMethods;
import it.unipd.dei.ims.terrier.utilities.StringUsefulMethods;
import it.unipd.dei.ims.terrier.utilities.UrlUtilities;
import it.unipd.dei.ims.terrier.utilities.UsefulConstants;

/** Phase 1 of the Blanco algorithm.
 * Given an RDF graph it converts its triples in trec documents.
 * That is, it creates multiple file trec containing multiple
 * documents. Each of these documents represent one triple.
 * 
 * */
public class RDFDatabaseToTRECConverter {

	/** Given a Relational database containing the triples that represents a RDF database,
	 * it converts the single triples in TREC documents.
	 * 
	 * @param databaseConnectiongString string to connect to the database where we have the triple store
	 * @param outputFile where to save the file TREC 
	 * */
	public static void rdfToTRECConverter(String databaseConnectingString, String outputDirectory) {
		//connect to the relational database
		Connection connection = null;

		//clean the directory
		try {
			FileUtils.cleanDirectory(new File(outputDirectory));
		} catch (IOException e1) {
			e1.printStackTrace();
			System.err.println("unable to clean the directory");
		}

		try {
			//connect to the database
			connection = DriverManager.getConnection(databaseConnectingString);

			//limit and offset for the select query
			int limit = 100000;
			int offset = 0;

			int counter = 0;
			int fileCounter = 0;


			//read all the triples one by one
			while(true) {
				//get the iterator over the triples
				ResultSet iterator = getTripleIteratorWithOffset(connection, limit, offset);
				//update the offset
				offset = offset + limit;
				//check we still have something to read
				if(iterator.first()) {
					//create a new file
					String outputFile = outputDirectory + "/" + (++fileCounter) + ".trec";
					//create the reader to our output file
					Path outputPath = Paths.get(outputFile);
					BufferedWriter writer = Files.newBufferedWriter(outputPath, UsefulConstants.CHARSET_ENCODING); 

					//get the cursor to the begin
					iterator.beforeFirst();
					while(iterator.next()) {
						writeOneDocument(iterator, writer);
						counter++;
					}//written a block of triples
					writer.flush();
					writer.close();
					System.out.println("written " + counter + " triples");
				} else {
					//we have read all the triples, we can exit
					break;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(connection!=null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

	/** Using the provided BufferedWriter, writes one document inside the TREC file
	 * */
	private static void writeOneDocument(ResultSet rs, BufferedWriter writer) throws SQLException, IOException {
		//read the next triple
		int id_ = rs.getInt(1);

		writer.write("<DOC>");
		writer.newLine();

		writer.write("<DOCNO>" + id_ + "</DOCNO>");
		writer.newLine();

		writeTheTriple(rs, writer);

		writer.write("</DOC>");
		writer.newLine();
	}

	private static void writeTheTriple(ResultSet rs, BufferedWriter writer) throws SQLException, IOException {
		String subject_ = rs.getString(2);
		String predicate_ = rs.getString(3);
		String object_ = rs.getString(4);

		//do the subject
		String s = UrlUtilities.takeFinalWordFromIRI(subject_);
		writer.write(s + " ");

		//do the predicate
		s = UrlUtilities.takeFinalWordFromIRI(predicate_);
		writer.write(s + " ");

		//do the object
		if(UrlUtilities.checkIfValidURL(object_)) {
			s = UrlUtilities.takeFinalWordFromIRI(object_);
			writer.write(s + " ");
		} else {
			s = StringUsefulMethods.getFirstPartOfRDFLiteral(object_);
			writer.write(s);
		}
		writer.newLine();



	}


	/** Returns a ResultSet (potentially empty) from the triple_store table, performing the query:
	 * <p>
	 * SELECT id_, subject_, predicate_, object_" + 
				"	FROM public.triple_store order by id_ ASC LIMIT ? OFFSET ?;";
				
	 * @param connection the Connection to the database.
	 * @param limit number of triplse to read
	 * @param offset starting from this offset, the method reads the triples.
	 * @throws SQLException 
	 * */
	private static ResultSet getTripleIteratorWithOffset(Connection connection, int limit, int offset) throws SQLException {
		String sql = "SELECT id_, subject_, predicate_, object_" + 
				"	FROM public.triple_store order by id_ ASC LIMIT ? OFFSET ?;";

		PreparedStatement stmt = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
		stmt.setInt(1, limit);
		stmt.setInt(2, offset);
		ResultSet rs = stmt.executeQuery();

		return rs;
	}

	//####################
	
	public static void main(String[] args) throws IOException {

		Map<String, String> map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/convertion.properties");

		String jdbcConnectionString = map.get("jdbc.connection.string");
		String outputTRECFile = map.get("output.trec.collection.directory");

		System.out.println("writing from " + jdbcConnectionString + " in directory " + outputTRECFile);

		RDFDatabaseToTRECConverter.rdfToTRECConverter(jdbcConnectionString, outputTRECFile);
		System.out.println("done");
	}
}
