package it.unipd.dei.ims.blanco.procedure;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;

import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.RDFParseException;
import org.openrdf.rio.RDFParser;
import org.openrdf.rio.Rio;
import org.openrdf.rio.helpers.StatementCollector;
import org.terrier.indexing.TRECCollection;
import org.terrier.structures.indexing.Indexer;
import org.terrier.structures.indexing.classical.BasicIndexer;

import it.unipd.dei.ims.rum.utilities.UrlUtilities;
import it.unipd.dei.ims.rum.utilities.UsefulConstants;
import it.unipd.dei.ims.terrier.utilities.BlazegraphUsefulMethods;
import it.unipd.dei.ims.terrier.utilities.PropertiesUsefulMethods;
import it.unipd.dei.ims.terrier.utilities.TerrierUsefulMethods;

/** Phase 6.5 of the Blanco Procedure. Here we create the R_j files and the different indexes that we need.
 * We also create a list of found predicates that are useful for the ranking phase.
 * */
public class RjPreparationPhase {

	//writes different R_j files in trec format
	public static void buildTheRjFiles () throws IOException {
		int counter = 0;
		Map<String, String> map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/blanco.properties");

		//directory where the collection is stored as RDF files (necessary to have the structure to build the Rj)
		String subgraphsDirectory = map.get("blanco.subgraphs.directory");
		File subgraphDirectoryFile = new File(subgraphsDirectory);

		String rjOutputDirectory = map.get("blanco.r.j.output.directory");

		Queue<File> subgraphDirQueue = new LinkedList<File>();
		Map<String, PrintWriter> rjMap = new HashMap<String, PrintWriter>();

		//traverse the tree of files in pre-order
		subgraphDirQueue.add(subgraphDirectoryFile);
		while(!subgraphDirQueue.isEmpty()) {
			File f = subgraphDirQueue.remove();
			File[] files = f.listFiles();
			for(File file : files) {
				if(file.isDirectory()) {
					subgraphDirQueue.add(file);
				} else {
					//it is a graph file
					String name = file.getName();
					if(name.equals(".DS_Store")) {
						continue;
					}
					addOneFileToRj(file, rjMap, rjOutputDirectory);
					counter++;
					if(counter%2048 == 0)
						System.out.println("printed " + counter + " graphs");
				}
			}
		}
		
		//print a list with all the predicates we have seen
		File d = new File(rjOutputDirectory).getParentFile();
		Path path = Paths.get(d.getAbsolutePath() + "/predicate_list.txt");
		BufferedWriter writer = Files.newBufferedWriter(path, UsefulConstants.CHARSET_ENCODING);

		//clean everything
		for(Entry<String, PrintWriter> entry : rjMap.entrySet()) {
			//print a line of the list
			String predicate = entry.getKey();
			writer.write(predicate);
			writer.newLine();
			
			//close one of the PrintWriter
			PrintWriter pw = entry.getValue();
			pw.println("");
			pw.print("</DOC>");
			pw.close();
		}
		
		writer.close();
	}

	/** Given the path of a RDF file, use it to increment the R_j documents.
	 * 
	 * @param rjMap a map containing the path for each file rj
	 * @param rjDirectory path to the directory where to save the files Rj
	 * */
	private static void addOneFileToRj(File graphFile, Map<String, PrintWriter> rjMap, String outputRjDirectory) {

		//read the file and incorporate in a model
		InputStream inputStream;
		try {
			File o1 = new File(outputRjDirectory);
			if(!o1.exists())
				o1.mkdirs();
			
			
			inputStream = new FileInputStream(graphFile);
			//prepare a collector to contain the triples
			StatementCollector collector = new StatementCollector();
			//read the file
			RDFParser rdfParser = Rio.createParser(RDFFormat.TURTLE);
			//link the collector to the parser
			rdfParser.setRDFHandler(collector);
			//parse the file
			rdfParser.parse(inputStream, "");
			//now get the triples/statements composing the graph
			Collection<org.openrdf.model.Statement> statements = collector.getStatements();
			Iterator<org.openrdf.model.Statement> iter = statements.iterator();

			while(iter.hasNext()) {
				//get the predicate r of the triple
				Statement t = iter.next();
				URI predicate = t.getPredicate();
				if(! rjMap.containsKey(predicate.toString()) ) {//first time we see this r
					//obtain the document connected to the statement
					String tripleDoc = BlazegraphUsefulMethods.fromStatementToDocument(t);
					//index it and obtained the version read through Terrier
					String title = UrlUtilities.takeFinalWordFromIRI(predicate.toString());

					//create a new file for this Rj document 
					String output = outputRjDirectory + "/" + title + ".trec";
					File o = new File(output);
					if(!o.exists())
						o.createNewFile();

					FileWriter fw = new FileWriter(output, true);
					BufferedWriter bw = new BufferedWriter(fw);
					PrintWriter out = new PrintWriter(bw);
					//open the document and write the first words
					out.println("<DOC>");
					out.println("<DOCNO>" + predicate.toString() + "</DOCNO>");
					out.print(tripleDoc + " ");

					//add the printer to the map
					rjMap.put(predicate.toString(), out);
					out.flush();
				} else {
//					predicate already present
					PrintWriter out = rjMap.get(predicate.toString()); 

					//get document of this triple
					String tripleDoc = BlazegraphUsefulMethods.fromStatementToDocument(t);
					//index it and obtained the version read throug Terrier
					String tDocTer = TerrierUsefulMethods.getDocumentWordsWithTerrierAsString(tripleDoc);
					//add the line to the file
					out.print(" " + tDocTer);
					out.flush();
				}

			}
		} catch ( RDFParseException | RDFHandlerException | IOException e) {
			e.printStackTrace();
		}
	}
	
	/** Creates one index for each document R_j in a separated directory. 
	 * Moreover, creates one index of the whole collection.
	 * */
	public static void createTheIndexes() throws IOException {
		System.out.println("creating the support indexes, please wait...");
		Map<String, String> map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/blanco.properties");
		
		//take the directory with the trec files
		String rjFilesDirectory = map.get("blanco.r.j.output.directory");
		//now create 1 index for each file
		File rjFir = new File(rjFilesDirectory);

		String indexesDirectory = rjFir.getParent() + "/rj_indexes";
		String inedexDirectory = rjFir.getParent() + "/rj_index";
		
		File[] files = rjFir.listFiles();
		List<org.terrier.indexing.Collection> colList = new ArrayList<org.terrier.indexing.Collection>();
		
//		String outputFileString = rjFir.getParent() + "/collection.etc";
//		Path out = Paths.get(outputFileString);
//		BufferedWriter writer = Files.newBufferedWriter(out, UsefulConstants.CHARSET_ENCODING);
		
		//index each and every R_j file
		for(File file : files) {
			//for each file, create the corresponding index
			if(file.getName().equals(".DS_Store"))
				continue;
			InputStream in = new FileInputStream(file);
			String outputDirIndex = indexesDirectory + "/" + file.getName().replaceAll("\\.trec", "");
			File dir = new File(outputDirIndex);
			if(!dir.exists())
				dir.mkdirs();
			
			org.terrier.indexing.Collection col = new TRECCollection(in);
//			Indexer indexer = new BasicIndexer(outputDirIndex, "data");
//			indexer.index(new org.terrier.indexing.Collection[]{ col });
			colList.add(col);
		}
		
		System.out.println("single indexes created. They are " + colList.size());
		
		//index the whole collection of R_j files
		org.terrier.indexing.Collection[] collectionArray = new org.terrier.indexing.Collection[colList.size()]; 
		for(int i = 0; i < colList.size(); ++i) {
			collectionArray[i] = colList.get(i);
		}
		
		File indxDir = new File(inedexDirectory);
		if(!indxDir.exists())
			indxDir.mkdirs();
		
		Indexer indexer = new BasicIndexer(inedexDirectory, "data");
		indexer.index(collectionArray);
		
	}
	
	//#########################

	public static void main(String[] args) throws IOException {
		//take the useful information by the properties file
		Map<String, String> map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/trec_ranking_phase.properties");

		//get the terrier home and etc directory where we have the property file and set them
		String terrierHome = map.get("terrier.home");
		String terrierEtc = map.get("terrier.etc");

		System.setProperty("terrier.home", terrierHome);
		System.setProperty("terrier.etc", terrierEtc);
		//create the files
		RjPreparationPhase.buildTheRjFiles();

		System.setProperty("indexer.meta.forward.keylens", "300");
//		System.setProperty("indexer.meta.forward.keys", "300");
		
		//index them
		RjPreparationPhase.createTheIndexes();
		
		System.out.println("Goodbye, and thanks for all the fish!");
	}
}
