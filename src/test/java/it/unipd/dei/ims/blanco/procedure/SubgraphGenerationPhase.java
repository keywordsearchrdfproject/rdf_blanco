package it.unipd.dei.ims.blanco.procedure;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import org.apache.commons.io.FileUtils;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.Value;
import org.openrdf.model.impl.StatementImpl;
import org.openrdf.model.impl.TreeModel;
import org.openrdf.model.impl.URIImpl;

import it.unipd.dei.ims.terrier.utilities.BlancoUsefuMethods;
import it.unipd.dei.ims.terrier.utilities.BlazegraphUsefulMethods;
import it.unipd.dei.ims.terrier.utilities.PropertiesUsefulMethods;
import it.unipd.dei.ims.terrier.utilities.SQLUtilities;
import it.unipd.dei.ims.terrier.utilities.TerrierUsefulMethods;
import it.unipd.dei.ims.terrier.utilities.UrlUtilities;

/** Phase 5 of the Blanco algorithm.
 * 
 * This phase utilizes the query graph E to build the different proposed subgraphs.
 * */
public class SubgraphGenerationPhase {

	private static final String SQL_SELECT_QUERY_GRAPH_TRIPLES = "SELECT id_, subject_, predicate_, object_ from query_graph order by id_ "
			+ "limit ? offset ?";
	
	private static int numberOfTriple = 0;
	private static int numberOfGraphs = 0;
	private static int directoryCounter = 0;
	
	
	/** Generates, provided a base graph E, the corresponding subgraphs.
	 * 
	 * @param jdbcConnectingString jdbc connection string to the support database
	 * @param queryDirectoryPath path of the directory containing all the data about the query
	 * @param query the query in keywords we are using to generate the subgraphs
	 * */
	public static void subgraphGenerationPhase(String jdbcConnectingString, String queryDirectoryPath, String query) {
		Connection connection = null;
		try {
			String subgraphsDirectory = queryDirectoryPath + "/blanco_support/subgraphs";
			File dir = new File(subgraphsDirectory);
			if(!dir.exists()) {
				dir.mkdirs();
			}
			
			//clear the directory of all the older subgraphs
			FileUtils.cleanDirectory(dir);
			
			//connect to the Relational Database
			connection = SQLUtilities.getRDBConnection(jdbcConnectingString);
			
			int limit = 500;
			int offset = 0;
			//list of the keywords
			List<String> keywords = TerrierUsefulMethods.getDocumentWordsWithTerrierAsList(query);
			
			//list to keep track of the graphs already produced and avoid the generation of subgraphs of subgraphs
			List<List<Integer>> subGraphList = new ArrayList<List<Integer>>();
			
			
			while(true) {
				//list all the triples in their id_ order
				PreparedStatement ps = connection.prepareStatement(SQL_SELECT_QUERY_GRAPH_TRIPLES, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
				ps.setInt(1, limit);
				ps.setInt(2, offset);
				
				ResultSet rs = ps.executeQuery();
				if(rs.next()) {
					rs.absolute(0);
					while(rs.next()) {
						//for each triple of the E graph
						int id_ = rs.getInt(1);
						//try to expand the subgraph starting from the seed triple
						++numberOfTriple;
						expandSubgraph(connection, id_, subGraphList, queryDirectoryPath, keywords);
					}
				}
				else
					break;
				//update the offset
				offset = offset + limit;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
//		} catch (IOException e) {
//			System.err.println("[ERROR] Problems cleaning the subgraphs directory");
//			e.printStackTrace();
//		} 
			finally {
		
		}
	}
	
	/**  The Blanco algorithm to expand a subgraph from the query graph E.
	 * <p>
	 * The graphs need to be unique and maximal. During the construction of a subgraph,
	 * we add only the triples with an id greater of the one of the triple. In this way we ensure
	 * the uniqueness of the graphs.
	 * <p>
	 * Also, we have to guarantee that all the graphs are maximal. To do so, we check every time
	 * that we produce a graph that it's not a subgraph of another graph already produced.
	 * 
	 * @param connection the connection to the database where to find the query graph E.
	 * @param id_ the id of the starting triple.
	 * @param keywords a list of keywords of the query to be used for the stopping condition of the algorithm
	 * 
	 * @throws SQLException 
	 * */
	private static void expandSubgraph(Connection connection, int id_, 
			List<List<Integer>> duplicateList, String queryDirectoryPath,
			List<String> keywords) throws SQLException {

		//create the model
		org.openrdf.model.Model model = new TreeModel();

		//a list to contain the words of the graph, in order to know when to stop
//		List<String> graphWordList = new ArrayList<String>();
		List<List<String>> keywordsSets = new ArrayList<List<String>>();
		
		//list of ID to keep track of the graphs to store.
		List<Integer> idList = new ArrayList<Integer>();
		
		//one queue to keep the triples of the cloud we are exploring
		Queue<Integer> cloudQueue = new LinkedList<Integer>();
		cloudQueue.add(id_);
		
		while(!cloudQueue.isEmpty()) {
			//take the id of this triple
			int id = cloudQueue.remove();
			//extrapolate the triple with the specified id
			Statement triple = getTripleFromId(connection, id);
			
			//create a list with the keywords from the triple
			List<String> tripleWordList = BlancoUsefuMethods.getWordsFromStatementToList(triple);
			
			//extrapolate the keywords contained in the triple
			List<String> tripleKeywordList = BlancoUsefuMethods.extrapolateKeywordsFormList(tripleWordList, keywords);
			
			
			//condition to add the triple as explaned in the paper
			if(!BlancoUsefuMethods.addTripleCondition(tripleKeywordList, keywordsSets))
				continue;
			
			//add the id of this triple to the list of added triples
			idList.add(id);
			
			//add the triple to the model
			model.add(triple);
			
			//add the words of the triple to the list of the words of the graph
//			BlancoUsefuMethods.addWordsFromStatementToList(triple, graphWordList);
			
			//add the keyword list of the triple to the set of keywords of this graph
			keywordsSets.add(tripleKeywordList);
			
			//add the neighbors of the triple to the queue
			BlancoUsefuMethods.addNeighboursToQueue(cloudQueue, triple, connection, id);
		}
		
		//check if the graph that we have created is subgraph of any other subgraph already created
		if(subgraphCondition(duplicateList, idList)) {
			//in this case, the graph is subgraph of another graph. We don't print it
			return;
		}
		
		numberOfGraphs++;
		//if we are here, we need to print the model in the directory
		String graphPath = queryDirectoryPath + "/blanco_support/subgraphs";
		File dir = new File(graphPath);
		if(!dir.exists())
			dir.mkdirs();
		
		//check if it is necessary to build a new directory
		if(numberOfGraphs==1 || numberOfGraphs%2048==0) {
			directoryCounter++;
			dir = new File(graphPath + "/" + directoryCounter);
			if(!dir.exists()) {
				dir.mkdirs();
			}
			System.out.println("checked out " + (numberOfTriple) + " triples, printed " + numberOfGraphs + " graphs");
		}
		
		//complete the path of the graph
//		graphPath = graphPath + "/" + directoryCounter + "/" + id_ + ".ttl";
		graphPath = graphPath + "/" + directoryCounter + "/" + numberOfGraphs + ".ttl";
		BlazegraphUsefulMethods.printTheDamnGraph(model, graphPath);
		
	}
	
	
	
	/** Creates a triple statement from the id of a triple in the relational database
	 * of the connection object.
	 * */
	private static Statement getTripleFromId(Connection connection, int id_) throws SQLException {
		//get the triple from the RDB
		String sql = "SELECT id_, subject_, predicate_, object_ from query_graph where id_=?;";
		PreparedStatement ps = connection.prepareStatement(sql);
		ps.setInt(1, id_);
		ResultSet rs = ps.executeQuery();
		rs.next();

		String sbj = rs.getString("subject_");
		String pr = rs.getString("predicate_");
		String obj = rs.getString("object_");

		//create the triple statement
		URI subject = new URIImpl(sbj);
		URI predicate = new URIImpl(pr);
		Value object;
		if(UrlUtilities.checkIfValidURL(obj)) {
			//obj is a URL
			object = new URIImpl(obj);
		} else {
			object = BlazegraphUsefulMethods.dealWithTheObjectLiteralString(obj);
		}

		Statement stat = new StatementImpl(subject, predicate, object);
		return stat;
	}
	
	/** given a list of lists and a new list, checks if the new list in contained or equal to 
	 * any of the other lists. 
	 * */
	private static boolean subgraphCondition(List<List<Integer>> duplicateList, List<Integer> idList) {
		for(List<Integer> greatList : duplicateList) {
			if(greatList.containsAll(idList)) {
				return true;
			}
		}
		return false;
	}
	
	
	//#################################
	
	
	public static void main(String[] args) throws IOException {
		//setting the same terrier.home and terrier.etc used previously in 
		//the indicisation of the graph and creation of the query graph E
		Map<String, String> map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/trec_index_phase.properties");
		String terrierHome = map.get("terrier.home");
		String terrierEtc = map.get("terrier.etc");
		System.setProperty("terrier.home", terrierHome);
		System.setProperty("terrier.etc", terrierEtc);
		
		map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/trec_subgraph_generation_phase.properties");
		String jdbcConnectionString = map.get("jdbc.connection.string");
		String queryDirectory = map.get("query.directory");
		String query = map.get("query");

		SubgraphGenerationPhase.subgraphGenerationPhase(jdbcConnectionString, queryDirectory, query);
		
		System.out.print("done");

	}

}
