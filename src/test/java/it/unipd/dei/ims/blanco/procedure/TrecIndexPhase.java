package it.unipd.dei.ims.blanco.procedure;

import java.io.IOException;
import java.util.Map;

import org.terrier.indexing.Collection;
import org.terrier.indexing.TRECCollection;
import org.terrier.structures.Index;
import org.terrier.structures.IndexOnDisk;
import org.terrier.structures.indexing.Indexer;
import org.terrier.structures.indexing.classical.BasicIndexer;

import it.unipd.dei.ims.terrier.utilities.PropertiesUsefulMethods;

/** Phase 2: the indexing of the TREC files in an index.
 * This class indexes trec files whose path are containet in
 * a collection.etc file. It is more efficient to index via batch, 
 * nonetheless.
 * */
public class TrecIndexPhase {


	public static void trecIndexPhase() throws IOException {

		//set the terrier properties
		Map<String, String> map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/trec_index_phase.properties");
		String terrierHome = map.get("terrier.home");
		String terrierEtc = map.get("terrier.etc");

		//setting system properties for terrier to find the terrier_home and the /etc directory
		System.setProperty("terrier.home", terrierHome);
		System.setProperty("terrier.etc", terrierEtc);

		String collectionFilename = map.get("collection.filename");
		String trecIndexPath = map.get("trec.index.path");

		Collection coll = new TRECCollection(collectionFilename);

		//indexing the collection
		Indexer indexer = new BasicIndexer(trecIndexPath, "data");
		indexer.index(new Collection[]{ coll });

		//the index is now created and open for reading
		Index index = IndexOnDisk.createIndex(trecIndexPath, "data");
		System.out.println("We have indexed " + index.getCollectionStatistics().getNumberOfDocuments() + " documents");
	}

	public static void main(String[] args) throws IOException {
		TrecIndexPhase.trecIndexPhase();
	}
}
