package it.unipd.dei.ims.blanco.procedure;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;
import java.util.Map.Entry;

import org.terrier.matching.ResultSet;
import org.terrier.querying.Manager;
import org.terrier.querying.SearchRequest;
import org.terrier.structures.Index;
import org.terrier.structures.IndexOnDisk;

import it.unipd.dei.ims.main.Utilities;
import it.unipd.dei.ims.terrier.utilities.PropertiesUsefulMethods;

/** Phase 3. Query the index with TF model to retrieve the triples that form the graph E.
 * Writing of the list in a file.
 * 
 * NB: this class needs a connection to database and the presence of a table metadata
 * where the triple_number (the total number of triples in the database) is specified. 
 * */
public class TrecQueryPhase {
	
	public static void trecQueryPhase() {
		
		Connection rdbConnection = null;
		
		//set the terrier properties
		try {
			Map<String, String> map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/trec_index_phase.properties");
			String indexPath = map.get("trec.index.path");
			String terrierHome = map.get("terrier.home");
			String terrierEtc = map.get("terrier.etc");

			//setting system properties for terrier to find the terrier_home and the /etc directory
			System.setProperty("terrier.home", terrierHome);
			System.setProperty("terrier.etc", terrierEtc);
			
			//open the index
			Index index = IndexOnDisk.createIndex(indexPath, "data");
			Manager queryingManager = new Manager(index);
			
			//where to save the results
			map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/query.properties");
			String jdbcConnectingString = map.get("jdbc.connecting.string");
			String resultDirectory = map.get("main.queries.directory");
			String model = map.get("model");
			
			//connect to the database to get the total number or triples
			rdbConnection = DriverManager.getConnection(jdbcConnectingString);
			String sql = "select triple_number from metadata;";
			
			//get the total number of triples in the dataset in order to correctly retrieve all the triples
			//containing a keyword
			PreparedStatement stmt = rdbConnection.prepareStatement(sql);
			java.sql.ResultSet rs = stmt.executeQuery();
			if(rs.next()) {
				int number_of_triples = rs.getInt(1);
				System.setProperty("trec.output.format.length", ""+number_of_triples);
				System.setProperty("matching.retrieved_set_size", ""+number_of_triples);
				//if we don't have this information, we go with the standard terrier length set to 1000
			}
			
			map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/query_list.properties");
			
			for(Entry<String, String> entry : map.entrySet()) {
				String id = entry.getKey();
				String query = entry.getValue();
				
				System.out.println("now doing query: " + query);
				
				SearchRequest srq = queryingManager.newSearchRequestFromQuery(query);
				srq.addMatchingModel("Matching" , model);
				
				queryingManager.runSearchRequest(srq);
				ResultSet results = srq.getResultSet();
				
				//path where to save the results of the query
//				Path outputPath = Paths.get(resultDirectory + "/" + id + ".res");
				String filePath = resultDirectory + "/" + id + "/blanco_support";
				File queryDir = new File(filePath);
				if(!queryDir.exists()) {
					queryDir.mkdirs();
				}
				//path of the file where to write the results
				Path outputPath = Paths.get(filePath + "/matching_triples.res");
				
				try (BufferedWriter writer = Files.newBufferedWriter(outputPath, Utilities.ENCODING)){
					System.out.println("Document Ranking");
					for (int i =0; i< results.getResultSize(); i++) {
						int docid = results.getDocids()[i];
						//identifier of the document
						String docno = index.getMetaIndex().getItem("docno", docid);
						double score = results.getScores()[i];
						writer.write("query_no " + id + " " + docno + " " + i + " " + score + " " + model);
						writer.newLine();
//						System.out.println("   Rank "+i+" docid: "+ docno + " "+" "+score);
					}
					writer.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		TrecQueryPhase.trecQueryPhase();
	}
}
