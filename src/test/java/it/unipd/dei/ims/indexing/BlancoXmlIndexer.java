package it.unipd.dei.ims.indexing;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.terrier.indexing.Collection;
import org.terrier.indexing.SimpleXMLCollection;
import org.terrier.structures.indexing.Indexer;
import org.terrier.structures.indexing.classical.BasicIndexer;

import it.unipd.dei.ims.main.Utilities;

/**This class uses a terrier.properties file to index
 * the xml files containing the documents D_j corresponding to the triple t_j
 * of the starting graph
 * */


public class BlancoXmlIndexer {
	
	public static void execute () {
		main(null);
	}

	public static void main (String[] args) {
		//getting the necessary data (path, properties etc.)
		Map<String, String> dataMap = Utilities.getData();

		//setting system properties for terrier to find the terrier_home and the /etc directory
		System.setProperty("terrier.home", dataMap.get(Utilities.TERRIER_HOME));
		System.setProperty("terrier.etc", dataMap.get(Utilities.TERRIER_ETC));


		//list of all the files what we want to be indexed 
		List<String> filesToProcess = new ArrayList<String>();
		//get path where xml files are stored
		String path = dataMap.get(Utilities.XML_FILE_DIRECTORY);
		pooulateListWithFiles(filesToProcess, path);

		//creating the collection
		Collection coll = new SimpleXMLCollection(filesToProcess);

		//indexing the collection
		Indexer indexer = new BasicIndexer(dataMap.get("index.1.directory"), dataMap.get(Utilities.BASIC_INDEX_NAME));
		indexer.index(new Collection[]{ coll });

	}


	/**
	 * Puts in the parameter list all the paths of the files in the directory
	 * path
	 * 
	 * @param path path of the directory where the xml files are stored*/
	private static void pooulateListWithFiles( List<String> list, String path ) {

		Path inputPath = Paths.get(path);
		List<Path> fileList = new ArrayList<Path>();

		try (Stream<Path> paths = Files.walk(inputPath)) {//read the files in the directory
			//create a list of paths
			//only with Java 8
			fileList = (List<Path>) paths.filter(Files::isRegularFile)
					.filter(s -> ! s.getFileName().startsWith(".DS_Store") )
					.collect(Collectors.toList());

			for( Path p: fileList) {
				list.add(p.toString());
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
