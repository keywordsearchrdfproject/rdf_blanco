package it.unipd.dei.ims.main;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.terrier.indexing.Collection;
import org.terrier.indexing.SimpleXMLCollection;
import org.terrier.matching.ResultSet;
import org.terrier.querying.Manager;
import org.terrier.querying.SearchRequest;
import org.terrier.structures.Index;
import org.terrier.structures.IndexOnDisk;
import org.terrier.structures.indexing.Indexer;
import org.terrier.structures.indexing.classical.BasicIndexer;

/**In this class we collect the xml document representing
 * the subgraphs of E retrieved by the Blanco retrieving algorithm
 * and ranking them with the LM  Blanco ranking model
 * 
 *
 * */
@Deprecated
public class BlancoFinalIndexerAndRanking {

	/**String identifying the xml file with all the retrieved documents from E*/
	private static final String XML_FILE = "xml_file";
	private static final String COLLECTION_SPEC = "collection.spec";
	private static final String TERRIER_HOME = "terrier_home";
	private static final String TERRIER_ETC = "terrier_etc";
	private static final String INDEX_DIRECTORY = "index_directory";
	private static final String INDEX_NAME = "index_name";
	private static final String QUERY = "query";

	private static final String MODEL = "BlancoElbassuoniLM";

	public static void main (String [] args) {

		//getting the necessary data (path, properties etc.)
		Map<String, String> dataMap = Utilities.getData();

		//setting system properties for terrier to find the terrier_home and the /etc directory
		System.setProperty("terrier.home", dataMap.get(TERRIER_HOME));
		System.setProperty("terrier.etc", dataMap.get(TERRIER_ETC));



		//list of all the files what we want to be indexed (in this case only one xml file)
		List<String> filesToProcess = new ArrayList<String>();
		filesToProcess.add(dataMap.get(XML_FILE));

		//creating the collection
		Collection coll = new SimpleXMLCollection(filesToProcess);


		//indexing the collection
		//XXX may require time, do it only once
		Indexer indexer = new BasicIndexer(dataMap.get("index.2.directory"), dataMap.get(INDEX_NAME));
		indexer.index(new Collection[]{ coll });

		//open the newly created index
		Index index = IndexOnDisk.createIndex(dataMap.get("index.2.directory"), dataMap.get(INDEX_NAME));
		System.out.println("We have indexed " + index.getCollectionStatistics().getNumberOfDocuments() + " documents");

		// Create a new manager run queries
		Manager queryingManager = new Manager(index);

		SearchRequest srq = queryingManager.newSearchRequestFromQuery(dataMap.get(QUERY));
		//        srq.addMatchingModel("Matching","DirichletLM");
		srq.addMatchingModel("Matching",MODEL);

		queryingManager.runSearchRequest(srq);

		ResultSet results = srq.getResultSet();

		// Print the results
		System.out.println(results.getExactResultSize()+" documents were scored");
		System.out.println("The top "+results.getResultSize()+" of those documents were returned");
		System.out.println("Document Ranking");

		Path outputPath = Paths.get(dataMap.get("subgraph.results.directory.path") + "final_ranking.res");

		try (BufferedWriter writer = Files.newBufferedWriter(outputPath, Utilities.ENCODING)){
			System.out.println("Document Ranking");
			for (int i =0; i< results.getResultSize(); i++) {
				int docid = results.getDocids()[i];

				String docno = index.getMetaIndex().getItem("docno", docid);
				double score = results.getScores()[i];
				writer.write("query_no Q0 " + docno + " " + i + " " + score + " " + MODEL);
				writer.newLine();
				System.out.println("   Rank "+i+" docno: "+ docno + " "+" "+score);
			}
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("done");
	}

}
