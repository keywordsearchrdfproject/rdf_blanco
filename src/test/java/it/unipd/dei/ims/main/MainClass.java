package it.unipd.dei.ims.main;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.jena.ext.com.google.common.base.Stopwatch;
import org.apache.log4j.Logger;

import it.unipd.dei.ims.indexing.BlancoXmlIndexer;
import it.unipd.dei.ims.precompute.FromRDFtoXMLFileConverter;
import it.unipd.dei.ims.precompute.FromRetrievedRDFGraphsToXMLDocumentConverter;
import it.unipd.dei.ims.precompute.UriDatabasePurifier;
import it.unipd.dei.ims.ranking.BlancoElbassuoniRanking;
import it.unipd.dei.ims.rdf.QueryGraphGenerator;
import it.unipd.dei.ims.rdf.SubgraphRetriever;

/** 
 * Starting class of the Blanco-Elbassuoni project.
 * 
 * 
 * */
@Deprecated
public class MainClass {

	static Logger log = Logger.getLogger(MainClass.class);
	
	private static Properties prop = new Properties();
	private static InputStream input = null;
	
	/**Starting method*/
	public static void main(String[] args) {
		
		/*first of all, take the .nt database and check that it is correctly written
		 * Create a new "corrected" version of the .nt database. */
		try {
			input = new FileInputStream("properties/terrier_data.properties");
			prop.load(input);
			
			log.info("Starting computation");
			
			Stopwatch timer = Stopwatch.createUnstarted();
			
			String property = prop.getProperty("purificationFlag", "false");
			
			if( property.equals("true")) {
				log.info("Check the dataset to control that triples are correctly written...");
				timer.start();
				//check the url in the RDF file
				UriDatabasePurifier.execute();
				log.info("Purification ended in " + timer.stop());
			}
			
			//convert to XML in order to have documents
			property = prop.getProperty("conversionFromRDFtoXMLflag", "false");
			if( property.equals("true")) {
				log.info("Starting conversion in xml documents");
				timer.start();
				//convert in xml files in order to index them
				FromRDFtoXMLFileConverter.execute();
				log.info("Conversion ended in " + timer.stop());
			}
			
			//Index the documents (long time required) 
			property = prop.getProperty("indexing.needed.1.label", "false");
			if( property.equals("true")) {
				log.info("Starting indexing xml documents");
				timer.start();
				//index the document
				BlancoXmlIndexer.execute();
				log.info("Indexing ended in " + timer.stop());
			}
			
			//XXX If you already have the index, start from here
			
			//rank the xml triples with the words of the query
			property = prop.getProperty("ranking.phase.1.label", "false");
			if( property.equals("true")) {
				log.info("Starting ranking xml documents");
				timer.start();
				//extrapolate triples from G that contains the words of the query
				//and write their ID down
				BlancoElbassuoniRanking.execute();
				log.info("Ranking ended in " + timer.stop());
			}
			
			//Generate the query graph E from the id of the retrieved triples
			property = prop.getProperty("generate.graph.E.label", "false");
			if( property.equals("true")) {
				log.info("Starting generating query graph E");
				timer.start();
				//Generate E
				QueryGraphGenerator.execute();
				log.info("Query graph E generated in " + timer.stop());
			}
			
			//creation of the subgraphs that are the answers to the query
			property = prop.getProperty("generate.subgraph", "false");
			if( property.equals("true")) {
				log.info("Starting generating subgraph of E");
				timer.start();
				SubgraphRetriever.execute();
				//we convert the .nt files in one big xml files containing all these documents
				FromRetrievedRDFGraphsToXMLDocumentConverter.main(null);
				log.info("Query graph E generated in " + timer.stop());
			}
			
			//aggiungi il ranking
			property = prop.getProperty("ranking.subgraph.label", "false");
			if( property.equals("true")) {
				log.info("Starting ranking subgraphs G ef E");
				timer.start();
				BlancoFinalIndexerAndRanking.main(null);
				log.info("Final ranking in " + timer.stop());
			}
			
			System.out.println("All done");
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
}
