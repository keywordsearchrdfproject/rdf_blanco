package it.unipd.dei.ims.main;

import java.io.IOException;
import java.util.Map;

import it.unipd.dei.ims.blanco.procedure.RDFDatabaseToTRECConverter;
import it.unipd.dei.ims.terrier.utilities.PropertiesUsefulMethods;

/** This is the first part of the Blanco procedure
 * <p>
 * #1*/
@Deprecated
public class RDFDatabaseToTRECConvertionExecutable {

	public static void main(String[] args) throws IOException {
		
		Map<String, String> map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/convertion.properties");
		
		String jdbcConnectionString = map.get("jdbc.connection.string");
		String outputTRECFile = map.get("output.trec.collection.directory");
		
		System.out.println("writing from " + jdbcConnectionString + " in directory " + outputTRECFile);
		
		RDFDatabaseToTRECConverter.rdfToTRECConverter(jdbcConnectionString, outputTRECFile);
		System.out.println("done");
	}

}
