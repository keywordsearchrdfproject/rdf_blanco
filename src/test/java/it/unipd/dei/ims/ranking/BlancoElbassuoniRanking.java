package it.unipd.dei.ims.ranking;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

import org.terrier.matching.ResultSet;
import org.terrier.querying.Manager;
import org.terrier.querying.SearchRequest;
import org.terrier.structures.Index;
import org.terrier.structures.IndexOnDisk;

import it.unipd.dei.ims.main.Utilities;

/**This class creates the result file from the ranking 
 * of the documents of the first index (index of
 * the xml documents  D_j obtained from the initial graph)
 * given one user query.
 * 
 * We use the simple DirichletLM to take the ranking.*/
public class BlancoElbassuoniRanking {

	public static void execute() {
		main(null);
	}
	
	public static void main (String[] args) {

		Map<String, String> dataMap = Utilities.getData();

		//always set the home and the /etc directory where the file terrier.properties is stored
		//otherwise the results can be different
		System.setProperty("terrier.home", dataMap.get(Utilities.TERRIER_HOME));
		System.setProperty("terrier.etc", dataMap.get(Utilities.TERRIER_ETC));

		//open the index
		Index index = IndexOnDisk.createIndex(dataMap.get("index.1.directory"), dataMap.get("index.1.name"));
		System.out.println("We have indexed " + index.getCollectionStatistics().getNumberOfDocuments() + " documents");

		Manager queryingManager = new Manager(index);
		SearchRequest srq = queryingManager.newSearchRequestFromQuery(dataMap.get(Utilities.QUERY));

		String model = dataMap.get("ranking.1.model");
		srq.addMatchingModel("Matching",model);

		queryingManager.runSearchRequest(srq);

		ResultSet results = srq.getResultSet();

		//usually, Terriers scored (returns as result of the scoring) a maximum of 1000 documents
		System.out.println(results.getExactResultSize()+" documents were scored");
		System.out.println("The top "+results.getResultSize()+" of those documents were returned");

		Path outputPath = Paths.get(dataMap.get("result.file.path"));

		try (BufferedWriter writer = Files.newBufferedWriter(outputPath, Utilities.ENCODING)){
			System.out.println("Document Ranking");
			for (int i =0; i< results.getResultSize(); i++) {
				int docid = results.getDocids()[i];
				//identifier of the document
				String docno = index.getMetaIndex().getItem("docno", docid);
				double score = results.getScores()[i];
				writer.write("query_no Q0 " + docno + " " + i + " " + score + " " + model);
				writer.newLine();
//				System.out.println("   Rank "+i+" docid: "+ docno + " "+" "+score);
			}
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
