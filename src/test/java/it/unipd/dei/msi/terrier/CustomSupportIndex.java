package it.unipd.dei.msi.terrier;

import java.util.ArrayList;
import java.util.List;

import org.terrier.indexing.Collection;
import org.terrier.indexing.SimpleXMLCollection;
import org.terrier.structures.Index;
import org.terrier.structures.IndexOnDisk;
import org.terrier.structures.Lexicon;
import org.terrier.structures.indexing.Indexer;
import org.terrier.structures.indexing.classical.BasicIndexer;
import org.terrier.utility.ApplicationSetup;

public class CustomSupportIndex {

	public static void main(String[] args) {
		//cartella di supporto etc
		System.setProperty("terrier.home", "/Users/dennisdosso/Documents/developer/terrier_versions/terrier-core");
		System.setProperty("terrier.etc", "/Users/dennisdosso/workspace-rdf/blanco_rdf/support_etc");
		List<String> filesToIndex = new ArrayList<String>();
		filesToIndex.add("/Users/dennisdosso/workspace-rdf/blanco_rdf/var/results/support/support_rj_documents.xml");
		
		Collection collection = new SimpleXMLCollection(filesToIndex);
		Indexer indexer = new BasicIndexer(
				"/Users/dennisdosso/workspace-rdf/blanco_rdf/var/results/support/support_index", 
				"data");
		indexer.index(new Collection[]{ collection });
		
		Index supportIndex = IndexOnDisk.createIndex(
				"/Users/dennisdosso/workspace-rdf/blanco_rdf/var/results/support/support_index",
				"data");
		System.out.println("We have indexed " + supportIndex.getCollectionStatistics().getNumberOfDocuments() + " documents R_j");
		Lexicon<String> lex = supportIndex.getLexicon();
	}
}
