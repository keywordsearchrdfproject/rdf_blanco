package it.unipd.dei.msi.terrier;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.terrier.indexing.Collection;
import org.terrier.indexing.TRECCollection;
import org.terrier.matching.ResultSet;
import org.terrier.querying.Manager;
import org.terrier.querying.SearchRequest;
import org.terrier.structures.Index;
import org.terrier.structures.IndexOnDisk;
import org.terrier.structures.indexing.Indexer;
import org.terrier.structures.indexing.classical.BasicIndexer;
import org.terrier.utility.ApplicationSetup;

import it.unipd.dei.ims.main.Utilities;

/**Testing class*/
public class IndexAndRankingTest {

	public static void main(String[] args) {

		System.setProperty("terrier.home", "/Users/dennisdosso/Documents/developer/terrier_versions/terrier-core");
		System.setProperty("terrier.etc", "/Users/dennisdosso/Documents/developer/terrier_versions/terrier-core/test_etc");
		
		InputStream input = null;
		try {
			input = new FileInputStream("/Users/dennisdosso/Documents/developer/terrier_versions/terrier-core/share/vaswani_npl/corpus/doc-text.trec");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Collection coll = new TRECCollection(input);

//		Indexer indexer = new BasicIndexer("/Users/dennisdosso/Documents/developer/terrier_versions/terrier-core/var/index", "data");
//		indexer.index(new Collection[]{ coll });
		
		//take the index
		Index index = IndexOnDisk.createIndex("/Users/dennisdosso/Documents/developer/terrier_versions/terrier-core/var/index", "data");
		
		Manager queryingManager = new Manager(index);
		//4.2
		SearchRequest srq = queryingManager.newSearchRequestFromQuery("MEASUREMENT OF DIELECTRIC CONSTANT OF LIQUIDS BY THE USE OF MICROWAVE TECHNIQUES");
		
		String model = "DirichletLM";
//		String model = "BlancoElbassuoniLM";
		srq.addMatchingModel("Matching",model);
		
		queryingManager.runSearchRequest(srq);

		ResultSet results = srq.getResultSet();
		
		System.out.println(results.getExactResultSize()+" documents were scored");
		System.out.println("The top "+results.getResultSize()+" of those documents were returned");

		Path outputPath = Paths.get("/Users/dennisdosso/Documents/developer/terrier_versions/terrier-core/var/results/eclipse-results.txt");

		try (BufferedWriter writer = Files.newBufferedWriter(outputPath, Utilities.ENCODING)){
			System.out.println("Document Ranking");
			for (int i =0; i< results.getResultSize(); i++) {
				int docid = results.getDocids()[i];
				String docno = index.getMetaIndex().getItem("docno", docid);
				double score = results.getScores()[i];
				writer.write("1 Q0 " + (docno) + " " + i + " " + score + " " + model);
				writer.newLine();
				System.out.println("   Rank " + i + " docid: "+ docno + " "+" "+score);
			}
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
