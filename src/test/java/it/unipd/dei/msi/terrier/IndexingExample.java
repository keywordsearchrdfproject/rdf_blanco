package it.unipd.dei.msi.terrier;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.terrier.indexing.Collection;
import org.terrier.indexing.SimpleFileCollection;
import org.terrier.structures.Index;
import org.terrier.structures.IndexOnDisk;
import org.terrier.structures.indexing.classical.BasicIndexer;

import it.unipd.dei.ims.terrier.utilities.PropertiesUsefulMethods;

/**Classe di test di terrier per vedere se fa tutto come promesso.
 * Eseguiamo lettura dei file da cartella, indicizzazione*/
public class IndexingExample {

	public static void main(String[] args) throws Exception {

		//take the useful information by the properties file
		Map<String, String> map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/trec_ranking_phase.properties");

		//get the terrier home and etc directory where we have the property file and set them
		String terrierHome = map.get("terrier.home");
		String terrierEtc = map.get("terrier.etc");

		System.setProperty("terrier.home", terrierHome);
		System.setProperty("terrier.etc", terrierEtc);
		System.setProperty("indexer.meta.forward.keylens", "300");

		// Directory containing files to index
		String aDirectoryToIndex = "/Users/dennisdosso/Documents/developer/terrier_versions/terrier-core/share/vaswani_npl/corpus/doc-text.trec";

		String destinationIndex = "/Users/dennisdosso/Documents/developer/terrier_versions/terrier-core/share/vaswani_npl/corpus_index";
		List<String> list = new ArrayList<String>();
		list.add(aDirectoryToIndex);

		BasicIndexer indexer = new BasicIndexer(destinationIndex, "data");
		Collection coll = new SimpleFileCollection(list, false);
		indexer.index(new Collection[]{coll});

		Index index = IndexOnDisk.createIndex(destinationIndex, "data");
		System.out.println("We have indexed " + index.getCollectionStatistics().getNumberOfDocuments() + " documents");
	}
}
