package it.unipd.dei.msi.terrier;

import java.io.IOException;
import java.util.Map;

import org.terrier.structures.DocumentIndex;
import org.terrier.structures.Index;
import org.terrier.structures.IndexOnDisk;
import org.terrier.structures.Lexicon;
import org.terrier.structures.LexiconEntry;
import org.terrier.structures.Pointer;
import org.terrier.structures.PostingIndex;
import org.terrier.structures.postings.IterablePosting;

import it.unipd.dei.ims.terrier.utilities.PropertiesUsefulMethods;

public class TerrierLexiconTest {

	public static void subgraphsRankingPhase() throws IOException {
		//take the useful information by the properties file
		Map<String, String> map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/trec_ranking_phase.properties");
		
		//get the terrier home and etc directory where we have the property file and set them
		String terrierHome = map.get("terrier.home");
		String terrierEtc = map.get("terrier.etc");
		
		System.setProperty("terrier.home", terrierHome);
		System.setProperty("terrier.etc", terrierEtc);
		
		//set the number of results we want 
		System.setProperty("trec.output.format.length", ""+1000);
		System.setProperty("matching.retrieved_set_size", ""+1000);
		
		
		String indexPath = map.get("trec.index.path");
		
		//open the index
		Index index = IndexOnDisk.createIndex(indexPath, "data");
		System.out.println("We have indexed " + index.getCollectionStatistics().getNumberOfDocuments() + " documents");
		
		//look for the words in the document of id docid
		PostingIndex<Pointer> di = (PostingIndex<Pointer>) index.getDirectIndex();
		DocumentIndex doi = index.getDocumentIndex();
		Lexicon<String> lex = index.getLexicon();
		int docid = 10; //docids are 0-based, so this is the 11-th document
		IterablePosting postings = di.getPostings(doi.getDocumentEntry(docid));
		while (postings.next() != IterablePosting.EOL) {
		    Map.Entry<String,LexiconEntry> lee = lex.getLexiconEntry(postings.getId());
		    System.out.println(lee.getKey() + " with frequency " + postings.getFrequency());
		}
		
		
	} 
	
	public static void main(String[] args) throws IOException {
		TerrierLexiconTest.subgraphsRankingPhase();
	}
	
}
