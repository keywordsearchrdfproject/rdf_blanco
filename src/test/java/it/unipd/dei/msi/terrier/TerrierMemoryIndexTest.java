package it.unipd.dei.msi.terrier;

import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

import org.terrier.indexing.Document;
import org.terrier.indexing.FileDocument;
import org.terrier.indexing.tokenisation.Tokeniser;
import org.terrier.matching.ResultSet;
import org.terrier.querying.Manager;
import org.terrier.querying.SearchRequest;
import org.terrier.realtime.memory.MemoryIndex;
import org.terrier.structures.DocumentIndex;
import org.terrier.structures.Lexicon;
import org.terrier.structures.LexiconEntry;
import org.terrier.structures.Pointer;
import org.terrier.structures.PostingIndex;
import org.terrier.structures.postings.IterablePosting;

import it.unipd.dei.ims.terrier.utilities.PropertiesUsefulMethods;

public class TerrierMemoryIndexTest {

	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws Exception {
		//take the useful information by the properties file
		Map<String, String> map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/trec_ranking_phase.properties");

		//get the terrier home and etc directory where we have the property file and set them
		String terrierHome = map.get("terrier.home");
		String terrierEtc = map.get("terrier.etc");

		System.setProperty("terrier.home", terrierHome);
		System.setProperty("terrier.etc", terrierEtc);

		//set the number of results we want 
		System.setProperty("trec.output.format.length", ""+1000);
		System.setProperty("matching.retrieved_set_size", ""+1000);
		
		String text = "Uao quella renna parla!";
		
		Document document = new FileDocument(new StringReader(text), new HashMap(), Tokeniser.getTokeniser());

		MemoryIndex memIndex = new MemoryIndex();

		memIndex.indexDocument(document);
		
		Manager queryingManager = new Manager(memIndex);
		SearchRequest srq = queryingManager.newSearchRequestFromQuery("my terrier query");
		srq.addMatchingModel("Matching","BM25");
		
		queryingManager.runSearchRequest(srq);
		
		ResultSet results = srq.getResultSet();

		PostingIndex<Pointer> di = (PostingIndex<Pointer>) memIndex.getInvertedIndex();
		DocumentIndex doi = memIndex.getDocumentIndex();
		Lexicon<String> lex = memIndex.getLexicon();
		
		//XXX here you can get the number of terms
		lex.numberOfEntries();
		//here you can get the terms
		lex.getIthLexiconEntry(3).getKey();
		
		int docid = 0; //docids are 0-based
		IterablePosting postings = di.getPostings(doi.getDocumentEntry(docid));
		while (postings.next() != IterablePosting.EOL) {
			Map.Entry<String,LexiconEntry> lee = lex.getLexiconEntry(postings.getId());
			System.out.println(lee.getKey() + " with frequency " + postings.getFrequency());
		}

		memIndex.close();

	}

}
